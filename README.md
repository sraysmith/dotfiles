# WELCOME ALL! THESE ARE MY PERSONAL DOTFILES!

Dotfiles are files that are hidden that generally pertain to user or system configuration settings. These dotfiles are customized for my workflow and may not work for everyone as is. Feel free to browse around and copy anything you like but just know that some dotfiles are dependent on other programs or scripts.

## WHAT MY SYSTEM LOOKS LIKE
![xmonad](screengrabs/my_xmonad_environment.png "my_xmonad_environment")
Depicted in the screenshot above is: htop, transmission-remote-cli, neofetch, ranger, my xmonad config, and neomutt. You can find this wallpaper [here](https://gitlab.com/sraysmith/wallpapers/blob/master/Spacescapes/blue-rose.jpg). You can also check out the rest of my wallpapers here at my [wallpaper repository](https://gitlab.com/sraysmith/wallpapers).


## WHAT I USE
I use the XMonad Tiling Window Manager with XMobar. As a baseline for the xmonad config, I borrowed elements from [Ethan Schoonover](https://github.com/altercation/dotfiles-tilingwm/blob/master/.xmonad/xmonad.hs). I have tried to use other TWM's such as i3wm, qtile, spectrwm but I always fall back to using xmonad. I use xmonad on both my desktop and my laptop. I custom built my own desktop with parts from Newegg. My laptop is a Lenovo Ideapad u530. I got a pretty good deal on it $300 I think I paid for it. I also have a 1st gen Surface Book but I removed Windows and put Arch on it with the gnome desktop and a kernel from [dmhacker](https://github.com/dmhacker/arch-linux-surface "dmhacker").

###### CLI PROGRAMS

- lynx (web browser)
- urxvt (terminal)
- ranger (file manager)
- nvim (text editor)
- khal (calendar)
- vdirsyncer (caldav server)
- taskwarrior (todo list)
- jrnl (journaling)
- transmission (torrenter)
- zathura (pdf viewer)
- newsboat (rss feeder)
- neomutt (mail client)
- dmenu w/rofi (menu launcher)
- dunst (notifications)
- compton (compositor)
- wordgrinder (distraction free writer)
- castnow (chromecast caster)
- sxiv (image viewer)
- pywal (color-scheme generator)
- yay (aur package manager)


###### GUI PROGRAMS

- qutebrowser (web browser)
- chromium (backup web browser)
- pcmanfm (file manager)
- libreoffice (office suite)
- lxappearance (gtk themer)
- inkscape (vector graphics)
- ckb-next (driver manager corsair)
- popcorntime.bin (movies)
- atom (development editor)
- gimp (image editor)
- shotcut (video editor)
- vokoscreen (camera)
- steam (gaming)
