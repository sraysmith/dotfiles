#!/bin/sh

#╔═╗╦═╗╔═╗╦ ╦╦  ╦╔╗╔╦ ╦═╗ ╦  ╔═╗╔═╗╔═╗╔╦╗  ╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦    ╔═╗╔═╗╦═╗╦╔═╗╔╦╗
#╠═╣╠╦╝║  ╠═╣║  ║║║║║ ║╔╩╦╝  ╠═╝║ ║╚═╗ ║   ║║║║╚═╗ ║ ╠═╣║  ║    ╚═╗║  ╠╦╝║╠═╝ ║
#╩ ╩╩╚═╚═╝╩ ╩╩═╝╩╝╚╝╚═╝╩ ╚═  ╩  ╚═╝╚═╝ ╩   ╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝  ╚═╝╚═╝╩╚═╩╩   ╩

#Author: Stephen Smith
#Date: 20191224
#Description: Easily install all my packages and configs

echo "STARTING ARCHLINUX POST INSTALL SCRIPT..."


# Packages
PKGS=(
    'alsa-plugins'                        # extras for alsa
    'alsa-utils'                          # linux sound support
    'arc-gtk-theme'                       # arc dark theme for gtk
    'atom'                                # electron text editor
    'atool'                               # manage archives
    'audacity'                            # audio manipulator program
    'autoconf'                            # a gnu tool for automatically configuring source code
    'automake'                            # gnu utility for make
    'biber'                               # unicode bibtex
    'bluez'                               # bluetooth daemon
    'bluez-libs'                          # bluetooth libraries
    'bluez-utils'                         # bluetooth development
    #'chromium'                            # web browser
    'clamav'                              # antivirus
    'cmake'                               # open-source make
    'cmatrix'                             # curses based matrix effect
    'cmus'                                # ncurses music player
    'cowsay'                              # talking cow
    'cups'                                # cups daemon
    'curl'                                # url retrieval utility
    'dex'                                 # launch desktop entry files
    'dialog'                              # dialog boxes for shell scripts
    'dictd'                               # online dictionary client and server
    'dmenu'                               # menu for x
    'dnsmasq'                             #
    'dunst'                               # notification daemon
    'ebtables'                            # ethernet bridge filtering utilities
    'electron'                            # development
    'espeak'                              # text to speak engine
    'fakeroot'                            # tool for emulating superuser privileges
    'feh'                                 # terminal based image manipulator
    'ffmpeg'                              # minimalist video player and cli utility
    'ffmpegthumbnailer'                   # ranger extension for video thumbnails
    'figlet'                              # make large letters with text
    'file-roller'                         # archive utility
    'flameshot'                           # screenshots
    'fortune-mod'                         # cli fortune cookie program
    'gcc'                                 # c/++ compiler
    'gimp'                                # gnu image manipulation program
    'git'                                 # version control system
    'glances'                             # cli monitoring tool
    'glibc'                               # c libraries
    'gnome-keyring'                       # store passwords and encryption keys
    'gvfs'                                # virtual filesystem for gio / needed for pcmanfm trash
    'gzip'                                # gnu compression utility
    'highlight'                           # cli source code highlighter
    'hplip'                               # hp printer drivers
    'htop'                                # process viewer
    'hugo'                                # static site generator
    'hunspell'                            # spellcheck libraries
    'i3lock-color'                        # screenlocker with color support
    'imagemagick'                         # cli manipulation tool
    'inkscape'                            # vector image creation
    'iw'                                  # cli configuration for wifi
    'irssi'                               # cli irc
    'jemalloc'                            # malloc implementation for cli-visualizer
    'jq'                                  # command-line json processor (open-weather)
    'keychain'                            # a front-end for ssh-agent
    'khal'                                # cli calendar
    'libnotify'                           # library for sending desktop notifications
    'libreoffice-fresh'                   # libreoffice with extra features
    'libsecret'                           # library for storing passwords
    'lm_sensors'                          # sensors for loading modules into kernel
    'lolcat'                              # colors in the terminal
    'lxappearance-gtk3'                   # gtk theme switcher
    'lynx'                                # cli web browser
    'make'                                # gnu make utility
    'man-db'                              # utility for reading man pages
    'man-pages'                           # man pages
    'mlocate'                             # merge locate/updatedb
    'mopidy'                              # music server like mpd
    'mpv'                                 # media player
    'mupdf'                               # pdf viewer
    'ncmpcpp'                             # music client
    'neofetch'                            # show system info
    'neomutt'                             # cli email
    'networkmanager'                      # network manager
    'network-manager-applet'              # network manager control for systray
    'newsboat'                            # cli rss reader
    'nextcloud-client'                    # desktop client for the nextcloud server
    'nodejs'                              # java runtime environment
    'npm'                                 # node package manager
    'openssh'                             # ssh tools
    'pacman-contrib'                      # pacman scripts
    'pandoc'                              # document conversion
    'parted'                              # disk utility
    'pavucontrol'                         # pulseaudio volume control
    'pcmanfm-gtk3'                        # files manager
    'perl-image-exiftool'                 # raw files reader for ranger
    'polkit-gnome'                        # polkit authentication
    'pulseaudio'                          # sound server
    'pulseaudio-alsa'                     # alsa config for pulseaudio
    'pulseaudio-bluetooth'                # bluetooth support for pulseaudio
    'pygmentize'                          # python syntax highlighter
    'python'                              # scripting language
    'python-pip'                          # python package installer
    'python-pywal'                        # generate colorscheme based on wallpaper
    'qemu'                                # machine emulator and virtualizer
    'qrencode'                            # library for qr code encoding
    'qutebrowser'                         # vim-like web browser based on pyqt5
    'ranger'                              # cli files manager
    'rofi'                                # window switcher, app launcher, and dmenu replacement
    'rsync'                               # backup utility
    #'rxvt-unicode'                        # unicode enabled terminal emulator
    'scrot'                               # screenshot utility
    'shotcut'                             # qt based video editor
    'sl'                                  # terminal steam locomotive
    'slop'                                # query for window selection
    'speedtest-cli'                       # internet speed via terminal
    'steam'                               # valve gaming library and delivery system
    'steam-native-runtime'                # native replacement for steam runtime using system libraries
    'stellarium'                          # database of sky-objects
    'sxiv'                                # image viewer
    'task'                                # taskwarrior
    'taskell'                             # haskell todo
    'terminator'                          # terminal emulator with tabs and grids / scratchpad
    'testdisk'                            # partition recovery
    'texlive-most'                        # LaTeX libraries
    'timew'                               # timewarrior for taskwarrior
    'transmission-cli'                    # cli for bittorrent client
    'trayer'                              # system tray for xmobar
    'ttf-font-awesome'                    # font awesome icons for xmobar
    'twolame'                             # mpeg audio encoder
    'udisks2'                             # disk management service needed for pcmanfm usb automount
    'ufw'                                 # gui firewall
    'unclutter'                           # hide mouse
    'unzip'                               # zip compression program
    'urlscan'                             # mutt and terminal url selector
    'urxvt-perls'                         # url and txt selection for urxvt
    'vdirsyncer'                          # caldav server
    'virt-manager'                        # virtual machine manager
    'virtualbox'                          # virtualization
    #'virtualbox-host-modules-arch'        # virtualization host kernel modules for arch
    'volumeicon'                          # volume control for systray
    'wpa_supplicant'                      # key negotiation for wpa wireless networks
    'w3m'                                 # text-based web browser
    'wget'                                # remote content retrievel
    'wmctrl'                              # cli controller for window manager
    'xcape'                               # configure modifier keys
    'xclip'                               # cli interface to x11 clipboard
    'xdotool'                             # cli x11 automation tool
    'xf86-video-amdgpu'                   # amd video driver
    'xmobar'                              # statusbar for xmonad
    'xmonad'                              # extensive window manager in haskell
    'xmonad-contrib'                      # extras needed for xmonad
    'xorg-server'                         # xorg server
    'xorg-xev'                            # print contents of x events
    'xorg-xinit'                          # xorg init
    'xorg-xkbcomp'                        # x keyboard description compiler
    'xorg-xrandr'                         # cli for randr
    'xorg-xrdb'                           # x resource database utility
    'xscreensaver'                        # screen saver utility for x
    'youtube-dl'                          # download youtube videos
    'zathura'                             # minimalist document reader
    'zathura-pdf-mupdf'                   # pdf support for zathura
    'zathura-ps'                          # post script support for zathura
    'zip'                                 # zip compression program
    'zsh'                                 # zshell
    'zsh-autosuggestions'                 # autosuggestions for zshell
)


# Checks to see if packages are installed and if not installs them.
for PKG in "${PKGS[@]}" ; do
    if pacman -Q ${PKG} ; then
       echo "The package ${PKG} is installed"
    else
       echo "Installing ${PKG}" && sudo pacman -S ${PKG} --noconfirm --needed
    fi
done


# AUR Packages
PKGS_AUR=(
    'bash-git-prompt'                     # git prompt for bash
    'castnow-git'                         # cli chromecast
    'ckb-next'                            # corsair drivers
    'cli-visualizer'                      # cli visualizer for cmus
    'compton-tryone-blackcapcoder-git'    # compton
    'crow-translate'                      # language translator
    'dict-foldoc'                         # the free online dictionary of computing for dictd et al
    'dict-freedict-deu-eng'               # Deutsch to English dictionary for dictd et al
    'dict-freedict-eng-deu'               # English to Deutsch dictionary for dictd et al
    'dict-freedict-eng-spa'               # English to Spanish dictionary for dictd et al
    'dict-freedict-spa-eng'               # Spanish to English dictionary for dictd et al
    'dict-gcide'                          # gnu version of international dictionary of english for dictd et al
    'dict-moby-thesaurus'                 # thesaurus for dictd et al
    'dict-wikt-en-all'                    # The English wiktionary for dictd et al
    'dict-wn'                             # WordNet for dictd et al
    'electronplayer'                      # stream movies scratchpad
    'gpmdp'                               # google play music scratchpad
    #'grub2-theme-vimix-legacy-git'        # grub theme
    'jellyfin-bin'                        # media server
    'jrnl'                                # cli journal
    'kjv-apocrypha'                             # cli bible
    'lib32-libxft-bgra'                   # glyph rendering
    'libxft-bgra'                         # glyph rendering
    'lightdm-mini-greeter'                # minimalist greeter
    'lorem-ipsum-generator'               # cli lorem ipsum
    'manuskript-git'                      # book writing
    'marker'                              # markdown editor
    'mconnect-git'                        # mobile connect for android
    'mopidy-mpd'                          # mpd plugin
    'mopidy-spotify'                      # spotify plugin
    'mutt-ics'                            # look at meeting notifications in neomutt
    'nerd-fonts-complete'                 # patched glyph fonts
    'no-more-secrets'                     # simulates data decryption effect
    #'numix-circle-icon-theme-git'         # numix circle theme
    #'numix-icon-theme-git'                # numix icon theme
    'otf-openmoji'                        # open source emoji
    'paper-icon-theme-git'                # paper icon theme
    'papirus-icon-theme'                  # papirus icon theme
    'popcorntime-bin'                     # stream torrents
    'pulseaudio-ctl'                      # volume control
    'pymodoro-git'                        # pomodoro for xmobar
    'python-wal-steam-git'                # generate colorscheme for steam based on wallpaper
    'rxvt-unicode-patched-with-scrolling' # patched for emoji
    'sardi-icons'                         # sardi icons
    'screenkey'                           # show key presses
    'simple-mtpfs'                        # read from mtp devices
    'synology-drive'                      # cloud backup to nas
    'task-spooler'                        # queue tasks
    'tootstream'                          # cli interface for mastodon
    'ttf-joypixels'                       # emoji
    'ttf-weather-icons'                   # weather font for xmobar
    'transmission-remote-cli-git'         # daemon for transmission torrent
    'unimatrix-git'                       # improved cmatrix
    'urxvt-config-reload-git'             # reload rxvt config
    'urxvt-resize-font-git'               # urxvt plugin to adjust font size
    'vim-live-latex-preview'              # vim plugin preview live pdf
    'vim-pathogen'                        # vim plugin manager
    'vokoscreenNG'                        # screencast
    'wordgrinder'                         # distraction-free writing
    'xkb-switch'                          # change xkb layouts
    'zsh-fast-syntax-highlighting'        # zshell syntax highlighting

)


# Checks to see if yay is installed and if not installs it.
if pacman -Q yay ; then
   echo "Yay is installed!"
else
   cd /tmp && git clone https://aur.archlinux.org/yay.git
   cd yay && makepkg -si && cd .. && rm -rf yay
fi


# Checks to see if AUR packages are installed and if not installs them.
for PKG in "${PKGS_AUR[@]}" ; do
    if pacman -Q ${PKG} ; then
	echo "The package ${PKG} is installed"
    else
	echo "Installing ${PKG}" && sudo -u $USER yay -S ${PKG} --noconfirm
    fi
done

echo "Done"
