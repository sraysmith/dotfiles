"╦┌┐┌┬┌┬┐╦  ╦┬┌┬┐
"║││││ │ ╚╗╔╝││││
"╩┘└┘┴ ┴o ╚╝ ┴┴ ┴

" Description :  My Neovim config.
" Author :       Stephen Smith
" Date :         20190516
" Plug Manager : ~/.local/share/nvim/site/autoload
" Plugins :      ~/.local/share/nvim/plugged

"""Plugin Manager"""{{{

" Automagically setup my plugs if not exist.
if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall

endif

" Plugins
call plug#begin('~/.local/share/nvim/plugged')
Plug 'preservim/nerdtree'                              "file explorer
Plug 'ron89/thesaurus_query.vim'                       "thesaurus
Plug 'tpope/vim-surround'                              "modify pairs
Plug 'godlygeek/tabular'                               "text alignment
Plug 'plasticboy/vim-markdown'                         "markdown syntax
Plug 'vim-scripts/Highlight-UnMatched-Brackets'        "matching pairs
Plug 'junegunn/goyo.vim'                               "distraction free
Plug 'LukeSmithxyz/vimling'                            "language helpers
Plug 'vimwiki/vimwiki'                                 "personal wiki
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'         "nerdtree syntax
Plug 'ryanoasis/vim-devicons'                          "icon glyps
Plug 'chrisbra/Colorizer'                              "css colors
Plug 'itchyny/lightline.vim'                           "lightline status
Plug 'itchyny/vim-gitbranch'                           "show branch name
Plug 'jreybert/vimagit'                                "ease git workflow
Plug 'mbbill/undotree'                                 "undo history
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }    "installs binary
Plug 'junegunn/fzf.vim'                                "fuzzy finder
Plug 'junegunn/limelight.vim'                          "hyperfocus
Plug 'tpope/vim-commentary'                            "multi comment out
Plug 'tpope/vim-surround'                              "surrounding marks
Plug 'alvan/vim-closetag'                              "close tags w/ >
Plug 'jiangmiao/auto-pairs'                            "autoclose marks
Plug 'francoiscabrol/ranger.vim'                       "ranger
Plug 'rbgrouleff/bclose.vim'                           "ranger dependency
Plug 'altercation/vim-colors-solarized'                "colorscheme
Plug 'dylanaraps/wal.vim'                              "pywal colorscheme"
call plug#end()
"}}}
"""Closetag"""{{{
"let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.erb,*.jsx,*tsx'
let g:closetag_emptyTags_caseSensitive = 1
"}}}
"""Goyo"""{{{
"Make text more readable while writing prose.
map <Leader>f :Goyo \| set bg=light \| set linebreak<CR>

"Make Goyo default for writing in Neomutt.
autocmd BufRead,BufNewFile /tmp/neomutt* let g:goyo_width=80
autocmd BufRead,BufNewFile /tmp/neomutt* :Goyo \| set bg=light
autocmd BufRead,BufNewFile /tmp/neomutt* map ZZ :Goyo\|x!<CR>
autocmd BufRead,BufNewFile /tmp/neomutt* map ZQ :Goyo\|q!<CR>
"}}}
"""Lightline"""{{{
"Enable tabline.
let g:lightline = {
      \ 'colorscheme': 'ayu_light',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name'
      \ },
      \ }
"}}}
"""Limelight""""{{{
"let g:limelight_conceal_ctermfg = 'gray'
let g:limelight_conceal_ctermfg = '240'

autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!
"}}}
"""NERDTree"""{{{
"Keybinding to toggle NERDTree.
map <C-n> :NERDTreeToggle<CR>

"Open NERDTree automatically if opening a directory.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

"Close nvim if the only window open is NERDTree.
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"Show hidden files.
let g:NERDTreeShowHidden = 1

"Hide Bookmarks label.
let g:NERDTreeMinimalUI = 1

"Prevent NERDTree from showing specific extensions.
let NERDTreeIgnore=['\.DS_Store', '\~$', '\.swp', '\.pyc$', '\.db$', '__pycache__', 'node_modules']
"}}}
"""Thesaurus Query"""{{{
"Accepts manual lookup through command mode :Thesuarus <phrase>.

"Disables default keybind <Leader>cs
let g:tq_map_keys = 0

"Keybinding to find and replace a word under the cursor.
nnoremap <leader>tr :ThesaurusQueryReplaceCurrentWord<CR>

"Keybinding to find and lookup a word under the cursor.
nnoremap <leader>tl :ThesaurusQueryLookupCurrentWord<CR>

"Keybinding to find and replace a multi-word phrase.
vnoremap <leader>tr y:ThesaurusQueryReplace<C-r>

"Enable mthesaur backend
let g:tq_mthesaur_file="~/.config/nvim/thesaurus/mthesaur.txt"
let g:tq_enabled_backends=["datamuse_com","mthesaur_txt"]
"}}}
"""UndoTree"""{{{
"Keybinding to toggle UndoTree.
nnoremap <leader>u :UndotreeToggle<CR>

"Show undotree and diff to the right
if !exists('g:undotree_WindowLayout')
    let g:undotree_WindowLayout = 3
endif
"}}}
"""Vim-Markdown"""{{{
"Syntax highlighting for yaml, toml, & json files in Hugo
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_toml_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1
"}}}
"""Vimling"""{{{
nm <Leader>d :call ToggleDeadKeys()<CR>
imap <Leader>d <esc>:call ToggleDeadKeys()<CR>a
nm <Leader>i :call ToggleIPA()<CR>
imap <Leader>i <esc>:call ToggleIPA()<CR>a
nm <Leader>q :call ToggleProse()<CR>
"}}}
"""Vimwiki"""{{{
"Read files.
let g:vimwiki_ext2syntax = {'.Rmd': 'markdown', '.rmd': 'markdown', '.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
let g:vimwiki_list = [{'path': '~/vimwiki', 'syntax': 'markdown', 'ext': '.md'}]
"}}}
"""Custom Commands"""{{{
"vim pathogen
execute pathogen#infect()

"Removes .tex files on exit.
autocmd VimLeave *.tex !texclear %

"Disables automatic comments.
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

"Deletes all trailing whitespace on exit.
autocmd BufWritePre * %s/\s\+$//e

"Runs xrdb whenever xresources is updated.
autocmd BufWritePost *xresources !xrdb %

"Runs whenever shortcuts are updated.
autocmd BufWritePost ~/.local/bin/folders,~/.local/bin/configs !shortcuts

"Ensure filetypes are read as either groff or tex
autocmd BufRead,BufNewFile *.ms,*.me,*.mom,*.man set filetype=groff
autocmd BufRead,BufNewFile *.tex set filetype=tex
"}}}
"""Folding"""{{{
set foldenable                      "enable folding
set foldlevelstart=10               "fold level to close
set foldnestmax=10                  "fold up to 10 levels
set foldmethod=syntax               "fold based on syntax

"Keybinding to toggle folds.
nnoremap <space> za
"}}}
"""General Settings"""{{{
let mapleader =","                  "leader key
set smartcase                       "case-sensitive search
set noswapfile                      "disable swap files
set nobackup                        "
set nowritebackup                   "
set undodir=~/.config/nvim/undodir  "
set undofile                        "
set modelines=1                     "ignore mode lines
set showmatch                       "highlight matching characters
set mouse=a                         "remove line numbers from clipboard
set incsearch                       "incremental search
set hlsearch                        "search highlighting
set path+=**                        "find files recursively
set wildmenu                        "tab complete menu
set wildmode=longest,list,full      "tab complete options
"}}}
"""Keybindings"""{{{
"To copy to both clipboard and primary selection.
vnoremap <C-c> "*y :let @+=@*<CR>

"Open my bibliography file in split
map <leader>b :vsp<space>$BIB<CR>
map <leader>r :vsp<space>$REFER<CR>

"Compile document in Groff/LaTeX/Markdown/etc.
map <leader>c :w! \| !compiler <c-r>%<CR>

"Open corresponding .pdf/.html or preview
map <leader>p :!opout <c-r>%<CR><CR>

"Highlight last inserted text.
nnoremap gV `[v`]

"Remap escape key.
inoremap ii <esc>
"}}}
"""Movement"""{{{
"Move to beginning/end of line.
nnoremap B ^
nnoremap E $

"$/^ doesn't do anything.
nnoremap $ <nop>
nnoremap ^ <nop>

"Navigating with guides.
inoremap <Space><Tab> <Esc>/<++><Enter>"_c4l
vnoremap <Space><Tab> <Esc>/<++><Enter>"_c4l
map <Space><Tab> <Esc>/<++><Enter>"_c4l

" Shortcutting split navigation, saving a keypress:
map <leader>h :wincmd h<CR>
map <leader>j :wincmd j<CR>
map <leader>k :wincmd k<CR>
map <leader>l :wincmd l<CR>

" Resize splits
nnoremap <Leader>+ :vertical resize +5<CR>
nnoremap <Leader>- :vertical resize -5<CR>
"}}}
"""Spelling"""{{{
set spell spelllang=en_us
set spellsuggest=best,10

"Toggles on/off spellcheck.
map <Leader>s :setlocal spell!<CR>

"Change highlighting.
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red
hi clear SpellRare
hi SpellRare cterm=underline ctermfg=red
hi clear SpellCap
hi SpellCap cterm=underline ctermfg=blue
hi clear SpellLocal
hi SpellLocal cterm=underline ctermfg=green
"}}}
"""TABS and Spacing"""{{{
set expandtab                       "tabs to spaces
set smarttab                        "insert tabstop
set smartindent                     "
set shiftwidth=4                    "shift is 4 spaces
set tabstop=4                       "v tab is 4 spaces
set softtabstop=4                   "i tab is 4 spaces
"}}}
"""UI Settings"""{{{
syntax enable                       "syntax highlighting
set lazyredraw                      "don't update screen
set number                          "show line numbers
set relativenumber                  "show current line
set showcmd                         "show last command
set rulerformat=%l,%c%V%=%P         "show position
set noshowmode                      "don't show status below
set splitbelow splitright           "split instruction
set fillchars+=vert:\               "border fill
set background=dark
"colorscheme solarized
colorscheme wal

"Highlight line containing cursor.
set cursorline
hi CursorLine cterm=NONE ctermbg=magenta ctermfg=white
hi ColorColumn cterm=NONE ctermbg=magenta ctermfg=white
hi Comment cterm=italic

augroup ColorcolumnOnlyInInsertMode
    autocmd!
    autocmd InsertEnter * setlocal colorcolumn=80
    autocmd InsertLeave * setlocal colorcolumn=0
augroup END

"hi Keyword ctermfg=darkcyan
"hi Constant ctermfg=5*
"hi Comment ctermfg=2*
"hi Normal ctermbg=none
hi LineNr ctermfg=darkgrey
"}}}
"""Snippets"""{{{

"""LATEX"""
	autocmd FileType tex map <leader>w :w !detex \| wc -w<CR>
	autocmd FileType tex inoremap ,bf \textbf{}<++><Esc>T{i
	autocmd FileType tex inoremap ,em \emph{}<++><Esc>T{i
	autocmd FileType tex inoremap ,it \textit{}<++><Esc>T{i
	autocmd FileType tex inoremap ,sc \textsc{}<Space><++><Esc>T{i
	autocmd FileType tex inoremap ,tt \texttt{}<Space><++><Esc>T{i
	autocmd FileType tex inoremap ,bt {\blindtext}
	autocmd FileType tex inoremap ,ct \textcite{}<++><Esc>T{i
	autocmd FileType tex inoremap ,cp \parencite{}<++><Esc>T{i
	autocmd FileType tex inoremap ,ref \ref{}<Space><++><Esc>T{i
	autocmd FileType tex inoremap ;a \href{}{<++>}<Space><++><Esc>2T{i
	autocmd FileType tex inoremap ,chap \chapter{}<Enter><Enter><++><Esc>2kf}i
	autocmd FileType tex inoremap ,sec \section{}<Enter><Enter><++><Esc>2kf}i
	autocmd FileType tex inoremap ,ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
	autocmd FileType tex inoremap ,sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
	autocmd FileType tex inoremap ,ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
	autocmd FileType tex inoremap ,ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
	autocmd FileType tex inoremap ,li <Enter>\item<Space>
	autocmd FileType tex inoremap ,tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
	autocmd FileType tex inoremap ,ot \begin{tableau}<Enter>\inp{<++>}<Tab>\const{<++>}<Tab><++><Enter><++><Enter>\end{tableau}<Enter><Enter><++><Esc>5kA{}<Esc>i
	autocmd FileType tex inoremap ,col \begin{columns}[T]<Enter>\begin{column}{.5\textwidth}<Enter><Enter>\end{column}<Enter>\begin{column}{.5\textwidth}<Enter><++><Enter>\end{column}<Enter>\end{columns}<Esc>5kA
	autocmd FileType tex inoremap ,x \begin{xlist}<Enter>\ex<Space><Enter>\end{xlist}<Esc>kA<Space>
	autocmd FileType tex inoremap ,fr \begin{frame}<Enter>\frametitle{}<Enter><Enter><++><Enter><Enter>\end{frame}<Enter><Enter><++><Esc>6kf}i


"""HTML"""
	autocmd FileType html inoremap ,b <b></b><Space><++><Esc>FbT>i
	autocmd FileType html inoremap ,it <em></em><Space><++><Esc>FeT>i
	autocmd FileType html inoremap ,gr <font color="green"></font><Esc>F>a
	autocmd FileType html inoremap ,rd <font color="red"></font><Esc>F>a
	autocmd FileType html inoremap ,yl <font color="yellow"></font><Esc>F>a
	autocmd FileType html inoremap ,1 <h1></h1><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,2 <h2></h2><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,3 <h3></h3><Enter><Enter><++><Esc>2kf<i
	autocmd FileType html inoremap ,p <p></p><Enter><Enter><++><Esc>02kf>a
	autocmd FileType html inoremap ,a <a<Space>href=""><++></a><Space><++><Esc>14hi
	autocmd FileType html inoremap ,e <a<Space>target="_blank"<Space>href=""><++></a><Space><++><Esc>14hi
	autocmd FileType html inoremap ,im <img src="" alt="<++>"><++><esc>Fcf"a
	autocmd FileType html inoremap ,ul <ul><Enter><li></li><Enter></ul><Enter><Enter><++><Esc>03kf<i
	autocmd FileType html inoremap ,ol <ol><Enter><li></li><Enter></ol><Enter><Enter><++><Esc>03kf<i
	autocmd FileType html inoremap ,li <Esc>o<li></li><Esc>F>a
	autocmd FileType html inoremap ,tab <table><Enter></table><Esc>O
	autocmd FileType html inoremap ,tr <tr></tr><Enter><++><Esc>kf<i
	autocmd FileType html inoremap ,th <th></th><++><Esc>Fhcit
	autocmd FileType html inoremap ,td <td></td><++><Esc>Fdcit
	autocmd FileType html inoremap ,dl <dl><Enter><Enter></dl><enter><enter><++><esc>3kcc
	autocmd FileType html inoremap ,dt <dt></dt><Enter><dd><++></dd><Enter><++><esc>2kcit


""".bib"""
	autocmd FileType bib inoremap ,a @article{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>journal<Space>=<Space>{<++>},<Enter>volume<Space>=<Space>{<++>},<Enter>pages<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
	autocmd FileType bib inoremap ,b @book{<Enter>author<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>6kA,<Esc>i
	autocmd FileType bib inoremap ,c @incollection{<Enter>author<Space>=<Space>{<++>},<Enter>title<Space>=<Space>{<++>},<Enter>booktitle<Space>=<Space>{<++>},<Enter>editor<Space>=<Space>{<++>},<Enter>year<Space>=<Space>{<++>},<Enter>publisher<Space>=<Space>{<++>},<Enter>}<Enter><++><Esc>8kA,<Esc>i
    autocmd FileType bib inoremap ,d @online{<Enter>title<Space>=<Space>"<++>",<Enter>author<Space>=<Space>"<++>",<Enter>year<Space>=<Space>"<++>",<Enter>url<Space>=<Space>"<++>",<Enter>urldate<Space>=<Space>"<++>"<Enter>}<Enter><++><Esc>7kA,<Esc>i


"""MARKDOWN"""
	autocmd Filetype markdown,rmd map <leader>w yiWi[<esc>Ea](<esc>pa)
	autocmd Filetype markdown,rmd inoremap ;b ****<++><Esc>F*hi
	autocmd Filetype markdown,rmd inoremap ,e **<++><Esc>F*i
	autocmd Filetype markdown,rmd inoremap ,s ~~~~<++><Esc>F~hi
	autocmd Filetype markdown,rmd inoremap ,1 #<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd inoremap ,2 ##<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd inoremap ,3 ###<Space><Enter><++><Esc>kA
	autocmd Filetype markdown,rmd inoremap ,i ![](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd inoremap ;a [](<++>)<++><Esc>F[a
	autocmd Filetype markdown,rmd inoremap ,n ---<Enter><Enter>
	autocmd Filetype rmd inoremap ,r ```{r}<CR>```<CR><CR><esc>2kO
	autocmd Filetype rmd inoremap ,p ```{python}<CR>```<CR><CR><esc>2kO
"}}}

" vim:foldmethod=marker:foldlevel=0:
