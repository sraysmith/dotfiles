config.source('../themes/default/base16-solarized-dark.config.py')
config.source('shortcuts.py')
config.load_autoconfig(False)
config.set("colors.webpage.darkmode.enabled", True)
