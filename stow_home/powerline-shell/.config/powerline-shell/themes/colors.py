from powerline_shell.themes.default import DefaultColor

# Get color definitions from https://jonasjacek.github.io/colors/

class Color(DefaultColor):
    USERNAME_FG = 30                 # Turquoise4
    USERNAME_BG = 184                # Yellow 3
    USERNAME_ROOT_BG = 1             # Maroon

    HOSTNAME_FG = 53                 # Deep Pink 4
    HOSTNAME_BG = 24                 # DeepSkyBlue4

    HOME_SPECIAL_DISPLAY = False
    HOME_FG = 147                    # LightSteelBlue
    HOME_BG = 189                    # LightSteelBlue1
    PATH_FG = 147                    # LightSteelBlue
    PATH_BG = 189                    # LightSteelBlue1
    CWD_FG = 147                     # LightSteelBlue
    SEPARATOR_FG = 14                # Aqua

    READONLY_BG = 1                  # Maroon
    READONLY_FG = 7                  # Silver

    REPO_CLEAN_FG = 14               # Aqua
    REPO_CLEAN_BG = 0                # Black
    REPO_DIRTY_FG = 3                # Olive
    REPO_DIRTY_BG = 0                # Black

    JOBS_FG = 4                      # Navy
    JOBS_BG = 8                      # Grey

    CMD_PASSED_FG = 15               # White
    CMD_PASSED_BG = 97               # MediumPurple3
    CMD_FAILED_FG = 15               # White
    CMD_FAILED_BG = 167              # IndianRed

    SVN_CHANGES_FG = REPO_DIRTY_FG
    SVN_CHANGES_BG = REPO_DIRTY_BG

    GIT_AHEAD_FG = 250               # Grey74
    GIT_AHEAD_BG = 240               # Grey35
    GIT_BEHIND_FG = 250              # Grey74
    GIT_BEHIND_BG = 240              # Grey35
    GIT_STAGED_FG = 15               # White
    GIT_STAGED_BG = 22               #
    GIT_NOTSTAGED_FG = 15            # White
    GIT_NOTSTAGED_BG = 130           # DarkOrange3
    GIT_UNTRACKED_FG = 15            # White
    GIT_UNTRACKED_BG = 52            # Dark Red
    GIT_CONFLICTED_FG = 15           # White
    GIT_CONFLICTED_BG = 9            # Red

    GIT_STASH_FG = 0                 # Black
    GIT_STASH_BG = 221               # LightGoldenrod2

    VIRTUAL_ENV_BG = 15              # White
    VIRTUAL_ENV_FG = 2               # Green

    AWS_PROFILE_FG = 7               # Silver
    AWS_PROFILE_BG = 2               # Green

    TIME_FG = 225                    # Thistle1
    TIME_BG = 242                    # Grey42

    BATTERY_Normal_FG = 15           # White
    BATTERY_Normal_BG = 35           # SpringGreen3
    BATTERY_LOW_FG = 15              # White
    BATTERY_LOW_BG = 178             # Gold3
