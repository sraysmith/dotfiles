-- ═╗ ╦╔╦╗┌─┐┌┐ ┌─┐┬─┐
-- ╔╩╦╝║║║│ │├┴┐├─┤├┬┘
-- ╩ ╚═╩ ╩└─┘└─┘┴ ┴┴└─
--
-- Description: XMobar status bar
-- Author:      Stephen Smith
-- Date:        20191217
-- Notes:       Place in ~/.config/xmonad

Config {
      font              = "xft:WeatherIcons:size=12,Ubuntu Nerd Font:size=8:italic:bold:antialias=true:hinting=true,FontAwesome5Free:style=Solid:pixelsize=18"
    , additionalFonts   = [ "xft:Joypixels:pixelsize=14,Inconsolata Nerd Font:pixelsize=16:antialias=true:hinting=true" ]
    , bgColor           = "BGCOLOR"
    , fgColor           = "FGCOLOR"
    , alpha             = 220                                             -- transparency level
    , position          = TopSize L 100 20
--    , position          = TopSize L 95 20                                  -- bar position / extra space reserved for trayer
    , allDesktops       = False                                            -- show on all desktops
    , overrideRedirect  = True                                             -- set the Override Redirect flag
    , commands          = [
          Run CoreTemp --Alias %coretemp%
            [ "-t","<fc=COLOR9><fn=1></fn></fc>  <core0>C°"
            , "-H", "60"
            , "-L", "40"
            , "-h", "COLOR1"
            , "-n", "11COLOR"
            , "-l", "COLOR2"
            , "-p", "4"
            ] 10
        , Run Memory --Alias %memory%
            [ "-t", "<fc=COLOR3><fn=1></fn></fc> <usedratio>%"
            , "-H", "90"
            , "-L", "20"
            , "-h", "COLOR1"
            , "-n", "11COLOR"
            , "-l", "COLOR2"
            , "-p", "4"
            ] 10
        , Run Battery --Alias %battery%
            [ "-t", "<acstatus>"
            , "-H", "85"
            , "-L", "20"
            , "-h", "COLOR2"
            , "-n", "11COLOR"
            , "-l", "COLOR1"
            , "-p", "5"
            , "--"                                                         -- battery specific options
            , "-o", "<fc=10COLOR><fn=1></fn></fc> <left>% (<timeleft>)"   -- discharging status
            , "-O", "<fc=11COLOR><fn=1></fn></fc> <left>%"                -- charging status
            , "-i", "<fc=COLOR2><fn=1></fn></fc> <left>%"                -- fully charged status
            , "-a", "notify-send -u critical 'CHARGE BATTERY NOW!!!'"      -- runs when battery percentage is <= -A
            , "-A", "15"                                                   -- threshold number 0 - 100 that triggers action -a
            ] 10
        , Run DynNetwork --Alias %dynnetwork%
            [ "-t", "<fc=COLOR5><fn=1> </fn><dev>: <fn=1>⬇️</fn><rx>    <fn=1>⬆️</fn><tx></fc>"
            , "-a", "l"
            , "-w", "4"
            , "-H", "5000" -- units: B/s
            , "-L", "1000" -- units: B/s
            , "-h", "COLOR2"
            , "-n", "11COLOR"
            , "-l", "COLOR1"
            , "-p", "4"
            , "-S", "True"
            , "--", "--devices", "wlp2s0,tun0"
            ] 10
        , Run Date "<fn=1>📅</fn><fc=13COLOR> %a %_d %b %Y</fc> | d.%j w.%W   <fn=1>🕖️</fn><fc=13COLOR> %H:%M:</fc>%S" "date" 10 --Alias %date%
        , Run Com "status-spotify" [] "spotify" 5
        , Run Com "status-vol" [] "volume" 5
        --, Run Com "status-net" [] "net" 10
        , Run Com "status-keyboard" [] "kb" 10
        , Run Com "screencast" ["status"] "screencast" 10
        , Run Com "openweathermap-detailed" [] "opw" 3600
        , Run CommandReader "pymodoro -p " "pomodoro"
        , Run StdinReader
        ]
        , sepChar            = "%"
        , alignSep           = "}{"

        , template           = "<fc=13COLOR>λ</fc> %StdinReader% } %date%    %opw%    %spotify%    {%screencast%  %coretemp%  %memory%  %battery%  %volume%  %pomodoro%  %dynnetwork%  "
    }

-- vim: ft=haskell:foldmethod=marker:foldlevel=0:expandtab:ts=4:shiftwidth=4
