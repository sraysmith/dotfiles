{-# LANGUAGE AllowAmbiguousTypes, DeriveDataTypeable, TypeSynonymInstances, MultiParamTypeClasses #-}

-- ╔═╗┌┬┐┌─┐┌─┐┬ ┬┌─┐┌┐┌┌─┐
-- ╚═╗ │ ├┤ ├─┘├─┤├┤ │││└─┐
-- ╚═╝ ┴ └─┘┴  ┴ ┴└─┘┘└┘└─┘
-- ═╗ ╦┌┬┐┌─┐┌┐┌┌─┐┌┬┐  ╔═╗┌─┐┌┐┌┌─┐┬┌─┐
-- ╔╩╦╝││││ ││││├─┤ ││  ║  │ ││││├┤ ││ ┬
-- ╩ ╚═┴ ┴└─┘┘└┘┴ ┴─┴┘  ╚═╝└─┘┘└┘└  ┴└─┘

-- Stephen Smith stephen.r.smith2@gmail.com
-- https://gitlab.com/sraysmith
-- 12/17/2021
-- current as of XMonad 0.17

-- IMPORTS                                   {{{
------------------------------------------------
{- To find information on haskell modules: https://www.haskell.org/hoogle/ -}

import XMonad
import Data.List
import qualified XMonad.StackSet as W

import XMonad.Actions.CopyWindow
import XMonad.Actions.DynamicProjects

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP


import XMonad.Layout.Magnifier
import XMonad.Layout.ThreeColumns


import XMonad.Prompt
-- import XMonad.Prompt.ConfirmPrompt


import XMonad.Util.Cursor
import XMonad.Util.EZConfig
import XMonad.Util.Loggers
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Util.Ungrab
import XMonad.Util.WorkspaceCompare


---------------------------------------------}}}
-- MAIN                                      {{{
------------------------------------------------
main :: IO ()
main = do
    xmproc <- spawnPipe myStatusBar
    xmonad
     -- . ewmhFullscreen
     $ dynamicProjects projects
     . ewmh
     -- . withEasySB (statusBarProp "xmobar" (pure myXmobarPP)) defToggleStrutsKey
     $ myConfig xmproc

myConfig p = def
    { borderWidth = myBorderWidth
    , clickJustFocuses = myClickJustFocuses
    , focusFollowsMouse = myFocusFollowsMouse
    , focusedBorderColor = myFocusedBorderColor
    -- , handleEventHook = myHandleEventHook
    , layoutHook = myLayoutHook
    -- , logHook = myXMobarPP
    , manageHook = myManageHook <+> manageDocks
    , modMask    = myModMask
    , normalBorderColor = myNormalBorderColor
    , startupHook = myStartupHook
    , terminal = myTerminal
    , workspaces = myWorkspaces
    }
  `additionalKeysP` myKeys
---------------------------------------------}}}
-- AUTOSTART                                 {{{
------------------------------------------------
myStartupHook = do
    spawnOnce "/home/ssmith/.config/xmonad/xmorun-laptop"
    setDefaultCursor xC_pirate
---------------------------------------------}}}
-- THEMES                                    {{{
------------------------------------------------
--config for displaying the prompt for projects and tags
warmPromptTheme = def
  { bgColor               = "#ffff66"
  , fgColor               = "#ff4466"
  , position              = Top
  }
---------------------------------------------}}}
-- VARIABLES                                 {{{
------------------------------------------------
myBorderWidth = 0
myClickJustFocuses = True
myFocusFollowsMouse = False
myFocusedBorderColor = "#5A5D0F"
myHandleEventHook = docks
myModMask = mod4Mask
myNormalBorderColor = "#DF03CA"
myStatusBar = "xmobar -x0 /home/ssmith/.config/xmonad/xmobarrcv17"
myTerminal = "urxvtc"
---------------------------------------------}}}
-- SCRATCHPADS                               {{{
------------------------------------------------

---------------------------------------------}}}
-- KEYBINDINGS                               {{{
------------------------------------------------
myKeys =
    [ ("M-S-z", spawn "xscreensaver-command -lock")
    , ("M-S-=", unGrab *> spawn "scrot -s"        )
    , ("M-<Return>"  , spawn myTerminal                   )
    , ("M-<Backspace>", kill)
    , ("M-w", switchProjectPrompt warmPromptTheme)
    , ("M-<Tab>", sendMessage NextLayout)
    ]
---------------------------------------------}}}
-- WORKSPACES                                {{{
------------------------------------------------
wsAV    = "AV"    --Audio/Visual
wsCOM   = "COM"   --Communications
wsDEV   = "DEV"   --Integrated Development Environment
wsDMO   = "DMO"   --Demo
wsFLOAT = "FLT"   --Floating
wsGEN   = "GEN"   --General
wsGIT   = "GIT"   --GitLab Master
wsMON   = "MON"   --Monitoring
wsNOV   = "NOV"   --Novel Creation
wsRW    = "RW"    --Read/Write .tex .md
wsSTM   = "STM"   --Steam Games
wsSYS   = "SYS"   --Systems
wsTMP   = "TMP"   --Temporary
wsWEB   = "WEB"   --Web Development
wsWRK   = "WRK"   --Work
wsXMD   = "XMD"   --XMonad DevelopmentD

myWorkspaces = [wsGEN, wsWRK, wsSYS, wsXMD, wsGIT, wsDEV, wsMON, wsRW, wsTMP]

projects :: [Project]
projects =
  [ Project { projectName      = "wsGEN"
            , projectDirectory = "~/"
            , projectStartHook = Nothing
            }
  ]

---------------------------------------------}}}
-- LOGHOOK                                   {{{
------------------------------------------------
-- myLogHook p = do
myXMobarPP :: PP
myXMobarPP = def
        -- { ppCurrent         = xmobarColor "#5aff0d" "" . wrap "[" "]"
        -- , ppTitle           = xmobarColor "#5aff0d" "" .shorten 30
        -- , ppVisible         = xmobarColor "#5aff0d" ""
        -- , ppUrgent          = xmobarColor "#5aff0d" "" . wrap "!" "!"
        -- , ppHidden          = xmobarColor "#5aff0d" ""
        -- , ppHiddenNoWindows = xmobarColor "#5aff0d" "" . const ""
        -- , ppSep             = xmobarColor "#5aff0d" "#5bcc00" " :: "
        -- , ppWsSep           = "<fc=#586e75> | </fc"
        -- , ppLayout          = xmobarColor "#5aff0d" ""
        -- , ppOrder           = \(ws:l:t:ex) -> [ws,l]++ex++[t]
        -- , ppExtras          = [windowCount]
        -- }
        { ppSep             = magenta " • "
        , ppTitleSanitize   = xmobarStrip
        , ppCurrent         = wrap " " "" . xmobarBorder "Top" "#8be9fd" 2
        , ppHidden          = white . wrap " " ""
        , ppHiddenNoWindows = lowWhite . wrap " " ""
        , ppUrgent          = red . wrap (yellow "!") (yellow "!")
        , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
        , ppExtras          = [logTitles formatFocused formatUnfocused]
        }
  where

    formatFocused   = wrap (white    "[") (white    "]") . magenta . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow

    -- | Windows should have *some* title, which should not not exceed a
    -- sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

    blue, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#ff79c6" ""
    blue     = xmobarColor "#bd93f9" ""
    white    = xmobarColor "#f8f8f2" ""
    yellow   = xmobarColor "#f1fa8c" ""
    red      = xmobarColor "#ff5555" ""
    lowWhite = xmobarColor "#bbbbbb" ""

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset

---------------------------------------------}}}
-- MANAGEHOOK                                {{{
------------------------------------------------
myManageHook :: ManageHook
myManageHook = composeAll
    [ className =? "Gimp" --> doFloat
    , isDialog            --> doFloat
    ]
---------------------------------------------}}}
-- LAYOUTS                                   {{{
------------------------------------------------
myLayoutHook = tiled ||| Mirror tiled ||| Full ||| threeCol
  where
    threeCol = magnifiercz' 1.3 $ ThreeColMid nmaster delta ratio
    tiled    = Tall nmaster delta ratio
    nmaster  = 1      -- Default number of windows in the master pane
    ratio    = 1/2    -- Default proportion of screen occupied by master pane
    delta    = 3/100  -- Percent of screen to increment by when resizing panes
---------------------------------------------}}}

-- TEMPLATE                                  {{{
------------------------------------------------

---------------------------------------------}}}

-- vim: ft=haskell:foldmethod=marker:foldlevel=0:expandtab:ts=4:shiftwidth=4
