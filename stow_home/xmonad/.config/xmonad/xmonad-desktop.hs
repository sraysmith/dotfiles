{-# LANGUAGE AllowAmbiguousTypes, DeriveDataTypeable, TypeSynonymInstances, MultiParamTypeClasses #-}
-- ╔═╗┌┬┐┌─┐┌─┐┬ ┬┌─┐┌┐┌┌─┐
-- ╚═╗ │ ├┤ ├─┘├─┤├┤ │││└─┐
-- ╚═╝ ┴ └─┘┴  ┴ ┴└─┘┘└┘└─┘
-- ═╗ ╦┌┬┐┌─┐┌┐┌┌─┐┌┬┐  ╔═╗┌─┐┌┐┌┌─┐┬┌─┐
-- ╔╩╦╝││││ ││││├─┤ ││  ║  │ ││││├┤ ││ ┬
-- ╩ ╚═┴ ┴└─┘┘└┘┴ ┴─┴┘  ╚═╝└─┘┘└┘└  ┴└─┘

-- Stephen Smith stephen.r.smith2@gmail.com
-- https://gitlab.com/sraysmith
-- 2019/12/11
-- current as of XMonad 0.15

-- IMPORTS                                   {{{
------------------------------------------------
{- To find information on haskell modules: https://www.haskell.org/hoogle/ -}

--Base
import Control.Monad
import Data.List
import qualified Data.Map as Map
import Data.Monoid
import System.Exit
import System.IO
import System.Posix.Process
import Colors  --Pywal Colors
import XMonad hiding ( (|||) )
import qualified XMonad.StackSet as W

--Actions
import XMonad.Actions.ConditionalKeys
import qualified XMonad.Actions.ConstrainedResize as Sqr
import XMonad.Actions.CopyWindow
import XMonad.Actions.CycleWS
import XMonad.Actions.DynamicProjects
import XMonad.Actions.DynamicWorkspaces
import XMonad.Actions.FloatSnap
import XMonad.Actions.MessageFeedback
import XMonad.Actions.Navigation2D
import XMonad.Actions.Promote
import XMonad.Actions.RotSlaves
import XMonad.Actions.SpawnOn
import XMonad.Actions.TagWindows
import XMonad.Actions.WithAll

--Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.DynamicProperty
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.FadeWindows
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.RefocusLast
import XMonad.Hooks.UrgencyHook

--Layouts
import XMonad.Layout.Accordion
import XMonad.Layout.BinarySpacePartition
import XMonad.Layout.DragPane
import XMonad.Layout.OneBig
import XMonad.Layout.ResizableTile
import XMonad.Layout.Simplest
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Tabbed
import XMonad.Layout.ThreeColumns
import XMonad.Layout.TwoPane

import XMonad.Layout.Dishes

--Layout Mods
import XMonad.Layout.BorderResize
import XMonad.Layout.BoringWindows
import XMonad.Layout.ComboP
import XMonad.Layout.Drawer
import XMonad.Layout.Fullscreen
import XMonad.Layout.Gaps
import XMonad.Layout.Hidden
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Magnifier
import XMonad.Layout.Master
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoFrillsDecoration
import XMonad.Layout.PerScreen
import XMonad.Layout.PerWorkspace
import XMonad.Layout.Reflect
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.TrackFloating
import XMonad.Layout.WindowNavigation

--Prompts
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt

--Utils
import XMonad.Util.Cursor
import XMonad.Util.EZConfig
import XMonad.Util.Loggers --ppextras (create logger)
import XMonad.Util.NamedActions
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedWindows
import XMonad.Util.Paste as P
import XMonad.Util.Run
import XMonad.Util.WorkspaceCompare
import XMonad.Util.XSelection
---------------------------------------------}}}
-- MAIN                                      {{{
------------------------------------------------
main = do

    xmproc <- spawnPipe myStatusBar
    xmonad
        $ dynamicProjects projects
        $ withNavigation2DConfig myNav2DConf
        $ withUrgencyHook LibNotifyUrgencyHook
        $ ewmh
        $ addDescrKeys' ((myModMask, xK_F1), showKeybindings) myKeys
        $ myconfig xmproc

myconfig p = def
        { borderWidth        = myBorderWidth
        , clickJustFocuses   = myClickJustFocuses
        , focusFollowsMouse  = myFocusFollowsMouse
        , normalBorderColor  = myNormalBorderColor
        , focusedBorderColor = myFocusedBorderColor
        , manageHook         = myManageHook
        , handleEventHook    = myHandleEventHook
        , layoutHook         = myLayoutHook
        , logHook            = myLogHook p
        , modMask            = myModMask
        , mouseBindings      = myMouseBindings
        , startupHook        = myStartupHook
        , terminal           = myTerminal
        , workspaces         = myWorkspaces
        }
---------------------------------------------}}}
-- AUTOSTART                                 {{{
------------------------------------------------
myStartupHook = do

    spawn "/home/ssmith/.config/xmonad/xmorun-desktop"
    --spawn "/home/ssmith/.config/xmonad/trayrc"

    setDefaultCursor xC_left_ptr

quitXmonad :: X ()
quitXmonad = io (exitWith ExitSuccess)

rebuildXmonad :: X ()
rebuildXmonad = do
    spawn "xmonad --recompile && xmonad --restart"

restartXmonad :: X ()
restartXmonad = do
    spawn "xmonad --restart"
---------------------------------------------}}}
-- THEME                                     {{{
------------------------------------------------
myFocusFollowsMouse  = False
myClickJustFocuses   = True

--Colors are defined in the Colors module
--base03  = "color8"
--base02  = "color0"
--base01  = "color10"
--base00  = "color11"
--base0   = "color12"
--base1   = "color14"
--base2   = "color7"
--base3   = "color15"
--yellow  = "color3"
--orange  = "color9"
--red     = "color1"
--magenta = "color5"
--violet  = "color13"
--blue    = "color4"
--cyan    = "color6"
--green   = "color2"

--sizes
gap            = 10
topbar         = 10
myBorderWidth  = 0
prompt         = 20
status         = 20

myNormalBorderColor     = "#000000"
myFocusedBorderColor    = active

active       = color4
activeWarn   = color1
inactive     = color0
focusColor   = color4
unfocusColor = color0

myFont      = "xft:Ubuntu Nerd Font:size13"
myBigFont   = "xft:Ubuntu Nerd Font:size16"
myWideFont  = "xft:Inconsolata Nerd Font:size14"
            ++ "style=Regular:pixelsize=170:hinting=true"

--config for the highlight bar for focused/unfocused windows
topBarTheme = def
    { fontName              = myFont
    , inactiveBorderColor   = inactive
    , inactiveColor         = inactive
    , inactiveTextColor     = inactive
    , activeBorderColor     = active
    , activeColor           = active
    , activeTextColor       = active
    , urgentBorderColor     = color1
    , urgentTextColor       = color3
    , decoHeight            = topbar
    }

--config for tabbed layouts
myTabTheme = def
    { fontName              = myFont
    , activeColor           = active
    , inactiveColor         = color0
    , activeBorderColor     = active
    , inactiveBorderColor   = color0
    , activeTextColor       = color8
    , inactiveTextColor     = color11
    }

--default config for all prompts
myPromptTheme = def
    { font                  = myFont
    , bgColor               = color8
    , fgColor               = active
    , fgHLight              = color8
    , bgHLight              = active
    , borderColor           = color8
    , promptBorderWidth     = 0
    , height                = prompt
    , position              = Top
    }

--config for displaying the prompt for projects and tags
warmPromptTheme = myPromptTheme
    { bgColor               = color3
    , fgColor               = color8
    , position              = Top
    }

--Config for displaying the prompt for exiting XMonad and to kill-all windows
hotPromptTheme = myPromptTheme
    { bgColor               = color1
    , fgColor               = color15
    , position              = Top
    }

--Config for displaying the workspace name when switching workspaces (X.L.ShowWName)
myShowWNameTheme = def
    { swn_font              = myWideFont
    , swn_fade              = 0.5
    , swn_bgcolor           = color8
    , swn_color             = color5
    }
---------------------------------------------}}}
-- APPLICATIONS                              {{{
------------------------------------------------
myTerminal          = "urxvtc"
myAltTerminal       = "cool-retro-term"
myBrowser           = "browser"
myBrowserClass      = "qutebrowser"
myStatusBar         = "xmobar -x0 /home/ssmith/.config/xmonad/xmobarrc-desktop"
myLauncher          = "rofi -show drun -modi 'drun,window,ssh' -sort -matching fuzzy -display-drun 'Applications' -display-window 'Windows' -display-ssh 'SSH'"
myUnicodeLauncher   = "rofi-unicode"
myEmojiLauncher     = "rofi-emoji"
myManLauncher       = "rofi-man"
mySearchLauncher    = "rofi-ducksearch"
myMountLauncher     = "rofi-mount"
myUnmountLauncher   = "rofi-umount"
myQrLauncher        = "xcqr"

windowCount :: X (Maybe String)
windowCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current . windowset
---------------------------------------------}}}
-- SCRATCHPADS                               {{{
------------------------------------------------
myScratchPads =
    [   (NS "taskellH" spawnTaskellH findTaskellH manageTaskellH)
    ,   (NS "taskellW" spawnTaskellW findTaskellW manageTaskellW)
    ,   (NS "music" spawnMusic findMusic manageMusic)
    ,   (NS "electronPlayer" spawnElectronPlayer findElectronPlayer manageElectronPlayer)
    ,   (NS "terminator" spawnTerminator findTerminator manageTerminator)
    ,   (NS "vokoscreen" spawnVokoscreen findVokoscreen manageVokoscreen)
    ,   (NS "notes" spawnNotes findNotes manageNotes)
    ,   (NS "calc" spawnCalc findCalc manageCalc)
    ]

    where
    spawnTaskellH = "urxvtc -name TaskellboardH -title taskellboardH -e taskell ~/.local/share/taskell/xmonad.md"
    findTaskellH = (className =? "URxvt") <&&> (stringProperty "WM_NAME" =? "taskellboardH")
    manageTaskellH = defaultFloating

    spawnTaskellW = "urxvtc -name TaskellboardW -title taskellboardW -e taskell ~/.local/share/taskell/work.md"
    findTaskellW = (className =? "URxvt") <&&> (stringProperty "WM_NAME" =? "taskellboardW")
    manageTaskellW = defaultFloating

    spawnMusic = "urxvtc -name Music -e ncmpcpp"
    findMusic = (resource =? "Music")
    manageMusic = defaultFloating

    spawnElectronPlayer = "electronplayer %u"
    findElectronPlayer = (className =? "electronplayer") <&&> (stringProperty "WM_WINDOW_ROLE" =? "browser-window")
    manageElectronPlayer = defaultFloating

    spawnTerminator = "terminator -T console -b -l Task --role=Scratchpad"
    findTerminator = (className =? "Terminator") <&&> (stringProperty "WM_WINDOW_ROLE" =? "Scratchpad")
    manageTerminator = defaultFloating

    spawnVokoscreen = "vokoscreenNG"
    findVokoscreen = (resource =? "vokoscreenNG")
    manageVokoscreen = (customFloating $ W.RationalRect (4.7/6) (3.2/5) (1/5) (1/3))

    spawnNotes = "urxvtc -name Notes -title vimwiki -e nvim ~/vimwiki/index.md"
    findNotes = (className =? "URxvt") <&&> (stringProperty "WM_NAME" =? "vimwiki")
    manageNotes = (customFloating $ W.RationalRect (4.7/6) (3.2/5) (1/5) (1/3))

    spawnCalc = "urxvtc -name Calc -e clac"
    findCalc = (className =? "URxvt") <&&> (stringProperty "WM_NAME" =? "clac")
    manageCalc = (customFloating $ W.RationalRect (4.7/6) (3.2/5) (1/5) (1/3))
---------------------------------------------}}}
-- KEYBINDINGS                               {{{
------------------------------------------------
myModMask = mod4Mask

-- Display keyboard mappings using zenity
showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
    h <- spawnPipe "zenity --title='XMonad Keybindings' --text-info --font=Ubuntu Nerd Font --width=500 --height=430 --no-wrap"
    hPutStr h (unlines $ showKm x)
    hClose h
    return ()


wsKeys = map show $ [1..9] ++ [0]


-- any workspace but scratchpad
notSP = (return $ ("NSP" /=) . W.tag) :: X (WindowSpace -> Bool)
shiftAndView dir = findWorkspace getSortByIndex dir (WSIs notSP) 1
        >>= \t -> (windows . W.shift $ t) >> (windows . W.greedyView $ t)


-- hidden, non-empty workspaces less scratchpad
shiftAndView' dir = findWorkspace getSortByIndexNoSP dir HiddenNonEmptyWS 1
        >>= \t -> (windows . W.shift $ t) >> (windows . W.greedyView $ t)
nextNonEmptyWS = findWorkspace getSortByIndexNoSP Next HiddenNonEmptyWS 1
        >>= \t -> (windows . W.view $ t)
prevNonEmptyWS = findWorkspace getSortByIndexNoSP Prev HiddenNonEmptyWS 1
        >>= \t -> (windows . W.view $ t)
getSortByIndexNoSP =
        fmap (.namedScratchpadFilterOutWorkspace) getSortByIndex


-- toggle any workspace but scratchpad
myToggle = windows $ W.view =<< W.tag . head . filter
        ((\x -> x /= "NSP" && x /= "SP") . W.tag) . W.hidden


myKeys conf = let

    subKeys str ks = subtitle str : mkNamedKeymap conf ks
    screenKeys     = ["w","v","z"]
    dirKeys        = ["j","k","h","l"]
    arrowKeys        = ["<D>","<U>","<L>","<R>"]
    dirs           = [ D,  U,  L,  R ]

    zipM  m nm ks as f = zipWith (\k d -> (m ++ k, addName nm $ f d)) ks as
    zipM' m nm ks as f b = zipWith (\k d -> (m ++ k, addName nm $ f d b)) ks as

    -- from xmonad.layout.sublayouts
    focusMaster' st = let (f:fs) = W.integrate st
        in W.Stack f [] fs
    swapMaster' (W.Stack f u d) = W.Stack f [] $ reverse u ++ d

    -- try sending one message, fallback if unreceived, then refresh
    tryMsgR x y = sequence_ [(tryMessage_ x y), refresh]

    -- do something with current X selection
    unsafeWithSelection app = join $ io $ liftM unsafeSpawn $ fmap (\x -> app ++ " " ++ x) getSelection

    toggleFloat w = windows (\s -> if Map.member w (W.floating s)
                    then W.sink w s
                    else (W.float w (W.RationalRect (1/3) (1/4) (1/2) (4/5)) s))

    in

    -----------------------------------------------------------------------
    -- System / Utilities                                                --
    -----------------------------------------------------------------------
    subKeys "System"
    [ ("<XF86AudioMute>"         , addName "Mute/Unmute Vol"  $ spawn "/usr/bin/pulseaudio-ctl mute")
    , ("<XF86AudioLowerVolume>"  , addName "Dec Vol"          $ spawn "/usr/bin/pulseaudio-ctl down")
    , ("<XF86AudioRaiseVolume>"  , addName "Inc Vol"          $ spawn "/usr/bin/pulseaudio-ctl up")
    , ("<XF86MonBrightnessDown>" , addName "Dec Scr Bright"   $ spawn "xbacklight - 5")
    , ("<XF86MonBrightnessUp>"   , addName "Inc Scr Bright"   $ spawn "xbacklight + 5")
    , ("M-<F7>"                  , addName "Session Opts"     $ spawn "rofi-session")
    , ("M-<F10>"                 , addName "Lock Screen"      $ spawn "colorlock")
    , ("M-<F11>"                 , addName "Swtch Kbd Layts"  $ spawn "kb_switcher")
    , ("M-q"                     , addName "Res XMD"          $ spawn "xmonad --restart")
    , ("M-C-q"                   , addName "Reb & res XMD"    $ spawn "xmonad --recompile && xmonad --restart")
    , ("M-S-q"                   , addName "Quit XMD"         $ confirmPrompt hotPromptTheme "Quit XMonad" $ io (exitWith ExitSuccess))
    , ("M-S-<F1>"                , addName "Disp Kbd"         $ spawn "zathura /home/ssmith/.config/xmonad/xmonadkeys.pdf")
    ] ^++^

    -----------------------------------------------------------------------
    -- Actions                                                           --
    -----------------------------------------------------------------------
    subKeys "Actions"
    [ ("M-<XF86Display>" , addName "Disp - Force Int"  $ spawn "rofi-displayctl internal")
    , ("M-o"             , addName "Disp (OP)"         $ spawn "rofi-displayctl menu")
    , ("M1-c"            , addName "Tog Compton"       $ spawn "comptog")
    , ("M1-p"            , addName "Capt Scr Full"     $ spawn "screenshot")
    , ("M1-S-p"          , addName "Capt Scr Focus W"  $ spawn "screenshot 'window'" )
    , ("M1-C-p"          , addName "Capt Scr Area"     $ spawn "screenshot 'area'" )
    , ("M1-r"            , addName "Rec Scr Full"      $ spawn "screencast" )
    , ("M1-S-r"          , addName "Rec Scr Area"      $ spawn "screencast area" )
    , ("M1-t"            , addName "Strt Pomo Timer"   $ spawn "touch ~/.pomodoro_session")
    , ("M1-S-t"          , addName "Stp Pomo Timer"    $ spawn "rm ~/.pomodoro_session")
    , ("M-/"             , addName "On-Scr Keys"       $ spawn "killall screenkey &>/dev/null || screenkey --no-systray")
    , ("M-S-/"           , addName "On-Scr Keys Opts"  $ spawn "screenkey --show-settings")
    , ("M-C-a"           , addName "Not X Clpbrd Sel"  $ unsafeWithSelection "notify-send")
    ] ^++^

    -----------------------------------------------------------------------
    -- Launchers                                                         --
    -----------------------------------------------------------------------
    subKeys "Launchers"
    [ ("M-<F2>"          , addName "ManPages"          $ spawn myManLauncher)
    , ("M-<F3>"          , addName "DuckSearch"        $ spawn mySearchLauncher)
    , ("M-<F4>"          , addName "Mount"             $ spawn myMountLauncher)
    , ("M-<F5>"          , addName "Unmount"           $ spawn myUnmountLauncher)
    , ("M-<F6>"          , addName "QR Code"           $ spawn myQrLauncher)
    , ("M-<Return>"      , addName "Terminal"          $ spawn myTerminal)
    , ("M-<Space>"       , addName "Apps"              $ spawn myLauncher)
    , ("M-S-<Space>"     , addName "Unicode"           $ spawn myUnicodeLauncher)
    , ("M-C-S-<Space>"   , addName "Emoji"             $ spawn myEmojiLauncher)
    , ("M-c"             , addName "NSP Calc"          $ namedScratchpadAction myScratchPads "calc")
    , ("M1-n"            , addName "NSP Console"       $ namedScratchpadAction myScratchPads "terminator")
    , ("M-m"             , addName "NSP Music"         $ namedScratchpadAction myScratchPads "music")
    , ("M-n"             , addName "NSP Notes"         $ namedScratchpadAction myScratchPads "notes")
    , ("M-t"             , addName "NSP Tasks"         $ bindOn WS [(wsWRK, namedScratchpadAction myScratchPads "taskellW"),
                                                         ("", namedScratchpadAction myScratchPads "taskellH")])
    , ("M-v"             , addName "NSP Video"         $ namedScratchpadAction myScratchPads "electronPlayer")
    , ("M1-x"            , addName "NSP Vokoscreen"    $ namedScratchpadAction myScratchPads "vokoscreen")
    , ("M-s"             , addName "Cancel Submap"     $ return ())
    , ("M-<KP_Insert>"   , addName "RangerFM"          $ spawn (myTerminal ++ " -e ranger"))
    , ("M-<KP_End>"      , addName "Lynx"              $ spawn (myTerminal ++ " -e lynx"))
    , ("M-<KP_Down>"     , addName "Neomutt"           $ spawn (myTerminal ++ " -e neomutt"))
    , ("M-<KP_Next>"     , addName "Htop"              $ spawn (myTerminal ++ " -e htop"))
    , ("M-<KP_Left>"     , addName "Cmus"              $ spawn (myTerminal ++ " -e cmus"))
    , ("M-<KP_Begin>"    , addName "Cli-Vis"           $ spawn (myTerminal ++ " -e vis"))
    , ("M-<KP_Right>"    , addName "Wordgrinder"       $ spawn (myTerminal ++ " -e wordgrinder ~/Documents/wgss.wg"))
    , ("M-<KP_Home>"     , addName "Newsboat"          $ spawn (myTerminal ++ " -e newsboat"))
    , ("M-<KP_Up>"       , addName "Irssi"             $ spawn (myTerminal ++ " -e irssi"))
    , ("M-<KP_Prior>"    , addName "XMD Conf"          $ spawn (myTerminal ++ " -e nvim ~/.config/xmonad/xmonad.hs"))
    , ("M-S-<KP_Insert>" , addName "PCManFM"           $ spawn "pcmanfm")
    , ("M-S-<KP_End>"    , addName "Qutebrowser"       $ spawn myBrowser)
    , ("M-S-<KP_Down>"   , addName "VirtMan"           $ spawn "virt-manager")
    , ("M-S-<KP_Next>"   , addName "Marker"            $ spawn "marker")
    , ("M-S-<KP_Left>"   , addName "Atom"              $ spawn "atom")
    , ("M-S-<KP_Begin>"  , addName "Gimp"              $ spawn "gimp")
    , ("M-S-<KP_Right>"  , addName "Shotcut"           $ spawn "shotcut")
    , ("M-S-<KP_Home>"   , addName "LibreOffice"       $ spawn "libreoffice")
    , ("M-S-<KP_Up>"     , addName "Inkscape"          $ spawn "inkscape")
    ] ^++^

    -----------------------------------------------------------------------
    -- Monitors                                                          --
    -----------------------------------------------------------------------
    subKeys "Monitors"
    [ ("M1-g"            ,addName "Cre UW Mon"         $ spawn "setmonitor")
    , ("M1-S-g"          ,addName "Del UW Mon"         $ spawn "delmonitor")
    ] ^++^

    -----------------------------------------------------------------------
    -- Windows                                                           --
    -----------------------------------------------------------------------
    subKeys "Windows"
    (
    [ ("M-p"             , addName "Hide W to Stack"   $ withFocused hideWindow)
    , ("M-S-p"           , addName "Res W to Stack"    $ popOldestHiddenWindow)
    , ("M-b"             , addName "Promote W"         $ promote)
    , ("M1-b"            , addName "Rotate Slave W"    $ rotSlavesUp)
    , ("M-d"             , addName "Dup W to All WS"   $ toggleCopyToAll)
    , ("M-<Backspace>"   , addName "Kill W"            kill1)
    , ("M-S-<Backspace>" , addName "Kill All W"        $ confirmPrompt hotPromptTheme "kill all" $ killAll)
    , ("M-S-g"           , addName "Mrge All to Subl"  $ withFocused (sendMessage . MergeAll))
    , ("M-g"             , addName "U-Mrge Frm Subl"   $ withFocused (sendMessage . UnMerge))
    , ("M-'"             , addName "Nav Tabs D"        $ bindOn LD [("Tabs", windows W.focusDown), ("", onGroup W.focusDown')])
    , ("M-;"             , addName "Nav Tabs U"        $ bindOn LD [("Tabs", windows W.focusUp), ("", onGroup W.focusUp')])
    , ("C-'"             , addName "Swp Tab D"         $ windows W.swapDown)
    , ("C-;"             , addName "Swp Tab U"         $ windows W.swapUp)
    , ("M1-S-a"          , addName "Add W Tags"        $ tagPrompt warmPromptTheme (\s -> withFocused (addTag s)))
    , ("M1-S-c"          , addName "Shft Tagd W to WS" $ tagPrompt warmPromptTheme (\s -> withTaggedGlobalP s shiftHere))
    , ("M1-S-d"          , addName "Del W Tags"        $ tagDelPrompt warmPromptTheme)
    , ("M-z u"           , addName "Focus Urgent"      focusUrgent)
    , ("M-z m"           , addName "Focus Master"      $ windows W.focusMaster)
    , ("M-z j"           , addName "Focus U"           $ windows W.focusUp)
    , ("M-z k"           , addName "Focus D"           $ windows W.focusDown)
    ]
    ++ zipM' "M-"        "Nav W"                       dirKeys dirs windowGo True
    ++ zipM' "C-"        "Move W"                      dirKeys dirs windowSwap True
    ++ zipM  "M-C-"      "Mrge w/Subl"                 dirKeys dirs (sendMessage . pullGroup)
    ++ zipM' "M-"        "Nav Scr"                     arrowKeys dirs screenGo True
    ++ zipM' "M-C-"      "Move W to Scr"               arrowKeys dirs windowToScreen True
    ++ zipM' "M-S-"      "Swp WS to Scr"               arrowKeys dirs screenSwap True
    ) ^++^

    -----------------------------------------------------------------------
    -- Workspaces & Projects                                             --
    -----------------------------------------------------------------------
    subKeys "Workspaces & Projects"
    (
    [ ("M-`"             , addName "Next Non-Emp WS"   $ nextNonEmptyWS)
    , ("M-S-`"           , addName "Prev Non-Emp WS"   $ prevNonEmptyWS)
    , ("M-a"             , addName "Tog Last WS"       $ toggleWS' ["NSP"])
    , ("M-w"             , addName "Swtch to Proj"     $ switchProjectPrompt warmPromptTheme)
    , ("M-S-w"           , addName "Shft to Proj"      $ shiftToProjectPrompt warmPromptTheme)
    ]
    ++ zipM "M-"         "View WS"                     wsKeys [0..] (withNthWorkspace W.greedyView)
    ++ zipM "C-"         "Move W to WS"                wsKeys [0..] (withNthWorkspace W.shift)
    ++ zipM "M-S-C-"     "Copy W to WS"                wsKeys [0..] (withNthWorkspace copy)
    ) ^++^

    -----------------------------------------------------------------------
    -- Layouts & Sublayouts                                              --
    -----------------------------------------------------------------------
    subKeys "Layout Management"
    [ ("M-<Tab>"         , addName "Cyc All Layts"     $ sendMessage NextLayout)
    , ("M-S-<Tab>"       , addName "Res Layt"          $ setLayout $ XMonad.layoutHook conf)
    , ("M-C-<Tab>"       , addName "Cyc Subl"          $ toSubl NextLayout)
    , ("M-f"             , addName "Fullscr"           $ sequence_ [ (withFocused $ windows . W.sink)
                                                       , (sendMessage $ XMonad.Layout.MultiToggle.Toggle FULL) ])
    , ("M-S-f"           , addName "Fake Fullscr"      $ sequence_ [ (P.sendKey P.noModMask xK_F11)
                                                       , (tryMsgR (ExpandTowards L) (Shrink))
                                                       , (tryMsgR (ExpandTowards R) (Expand)) ])
    , ("M-y"             , addName "Flt Tiled W"       $ withFocused toggleFloat)
    , ("M-S-y"           , addName "Tile All Fltng W"  $ sinkAll)
    , ("M-r"             , addName "Ref/Rot"           $ tryMsgR (Rotate) (XMonad.Layout.MultiToggle.Toggle REFLECTX))
    , ("M-S-r"           , addName "Frce Ref (BSP)"    $ sendMessage (XMonad.Layout.MultiToggle.Toggle REFLECTX))
    , ("C-S-h"           , addName "Ctrl-H Passthr"    $ P.sendKey controlMask xK_h)
    , ("C-S-j"           , addName "Ctrl-J Passthr"    $ P.sendKey controlMask xK_j)
    , ("C-S-k"           , addName "Ctrl-K Passthr"    $ P.sendKey controlMask xK_k)
    , ("C-S-l"           , addName "Ctrl-L Passthr"    $ P.sendKey controlMask xK_l)
    ] ^++^

    -----------------------------------------------------------------------
    -- Resizing                                                          --
    -----------------------------------------------------------------------
    subKeys "Resize"
    [ ("M-,"             , addName "Dec Master W"      $ sendMessage (IncMasterN (-1)))
    , ("M-."             , addName "Inc Master W"      $ sendMessage (IncMasterN 1))
    , ("M-["             , addName "Exp (L on BSP)"    $ tryMsgR (ExpandTowards L) (Shrink))
    , ("M-]"             , addName "Exp (R on BSP)"    $ tryMsgR (ExpandTowards R) (Expand))
    , ("M-S-["           , addName "Exp (U on BSP)"    $ tryMsgR (ExpandTowards U) (MirrorShrink))
    , ("M-S-]"           , addName "Exp (D on BSP)"    $ tryMsgR (ExpandTowards D) (MirrorExpand))
    , ("M-C-["           , addName "Shrk (L on BSP)"   $ tryMsgR (ShrinkFrom R) (Shrink))
    , ("M-C-]"           , addName "Shrk (R on BSP)"   $ tryMsgR (ShrinkFrom L) (Expand))
    , ("M-C-S-["         , addName "Shrk (U on BSP)"   $ tryMsgR (ShrinkFrom D) (MirrorShrink))
    , ("M-C-S-]"         , addName "Shrk (D on BSP)"   $ tryMsgR (ShrinkFrom U) (MirrorExpand))
    ]
        where
        toggleCopyToAll = wsContainingCopies >>= \ws -> case ws of

            [] -> windows copyToAll
            _ -> killAllOtherCopies
---------------------------------------------}}}
-- MOUSEBINDINGS                             {{{
------------------------------------------------
myMouseBindings (XConfig {XMonad.modMask = myModMask}) = Map.fromList $

    [ ((myModMask,               button1) ,(\w -> focus w
      >> mouseMoveWindow w
      >> ifClick (snapMagicMove (Just 50) (Just 50) w)
      >> windows W.shiftMaster))

    , ((myModMask .|. shiftMask, button1), (\w -> focus w
      >> mouseMoveWindow w
      >> ifClick (snapMagicResize [L,R,U,D] (Just 50) (Just 50) w)
      >> windows W.shiftMaster))

    , ((myModMask,               button3), (\w -> focus w
      >> mouseResizeWindow w
      >> ifClick (snapMagicResize [R,D] (Just 50) (Just 50) w)
      >> windows W.shiftMaster))

    , ((myModMask .|. shiftMask, button3), (\w -> focus w
      >> Sqr.mouseResizeWindow w True
      >> ifClick (snapMagicResize [R,D] (Just 50) (Just 50) w)
      >> windows W.shiftMaster ))

    ]
---------------------------------------------}}}
-- WORKSPACES                                {{{
------------------------------------------------
wsAV    = "AV"    --Audio/Visual
wsCOM   = "COM"   --Communications
wsDEV   = "DEV"   --Integrated Development Environment
wsDMO   = "DMO"   --Demo
wsFLOAT = "FLT"   --Floating
wsGEN   = "GEN"   --General
wsGIT   = "GIT"   --GitLab Master
wsMON   = "MON"   --Monitoring
wsNOV   = "NOV"   --Novel Creation
wsRW    = "RW"    --Read/Write .tex .md
wsSTM   = "STM"   --Steam Games
wsSYS   = "SYS"   --Systems
wsTMP   = "TMP"   --Temporary
wsWEB   = "WEB"   --Web Development
wsWRK   = "WRK"   --Work
wsXMD   = "XMD"   --XMonad Development

-- myWorkspaces = map show [1..9]
myWorkspaces = [wsGEN, wsWRK, wsSYS, wsXMD, wsGIT, wsDEV, wsMON, wsRW, wsTMP]

projects :: [Project]
projects =

    [ Project   { projectName       = wsAV
                , projectDirectory  = "~/Music"
                , projectStartHook  = Just $ do runInTerm "-title 'my visualizer'" "vis"
                                                spawnOn wsAV "urxvtc -e ncmpcpp -q"
                                                spawnOn wsAV "urxvtc -e bum --size 75"
                }

    , Project   { projectName       = wsDEV
                , projectDirectory  = "~/Projects"
                , projectStartHook  = Just $ do spawnOn wsDEV "/usr/bin/atom"
                }

    , Project   { projectName       = wsDMO
                , projectDirectory  = "~/"
                , projectStartHook  = Just $ do spawn "/usr/lib/xscreensaver/spheremonics"
                                                runInTerm "-name top" "top"
                                                runInTerm "-name top" "htop"
                                                runInTerm "-name glances" "glances"
                                                spawn "/usr/lib/xscreensaver/cubicgrid"
                                                spawn "/usr/lib/xscreensaver/surfaces"
                }

    , Project   { projectName       = wsGEN
                , projectDirectory  = "~/"
                , projectStartHook  = Nothing
                }

    , Project   { projectName       = wsGIT
                , projectDirectory  = "~/gitlab/dotfiles"
                , projectStartHook  = Just $ do spawnOn wsGIT myTerminal
                                                runInTerm "-name ranger" "ranger"
                }

    , Project   { projectName       = wsMON
                , projectDirectory  = "~/"
                , projectStartHook  = Just $ do runInTerm "-name htop" "htop"
                }

    , Project   { projectName       = wsNOV
                , projectDirectory  = "~/Documents/Novels"
                , projectStartHook  = Just $ do spawnOn wsNOV "~/.local/share/bibisco/bibisco"
                }

    , Project   { projectName       = wsSTM
                , projectDirectory  = "~/"
                , projectStartHook  = Just $ do spawnOn wsSTM "steam-runtime"
                }

    , Project   { projectName       = wsSYS
                , projectDirectory  = "~/"
                , projectStartHook  = Just $ do spawnOn wsSYS myTerminal
                                                spawnOn wsSYS myTerminal
                                                spawnOn wsSYS myTerminal
                }

    , Project   { projectName       = wsTMP
                , projectDirectory  = "/tmp"
                , projectStartHook  = Just $ do return ()
                }

    , Project   { projectName       = wsWEB
                , projectDirectory  = "~/Projects/website/sraysmith"
                , projectStartHook  = Just $ do spawnOn wsWEB myTerminal
                                                runInTerm "-name ranger" "ranger"
                }

    , Project   { projectName       = wsWRK
                , projectDirectory  = "~/Work"
                , projectStartHook  = Just $ do spawnOn wsWRK myBrowser
                                                spawnOn wsWRK "sleep 2; urxvtc"
                }

    , Project   { projectName       = wsXMD
                , projectDirectory  = "~/.config/xmonad"
                , projectStartHook  = Just $ do runInTerm "-title xmonad.hs" "nvim ~/.config/xmonad/xmonad.hs"
                                                spawnOn wsXMD "sleep 1; urxvtc"
                                                spawnOn wsXMD "sleep 1; urxvtc"
                }
    ]
---------------------------------------------}}}
-- LAYOUTS                                   {{{
------------------------------------------------
myNav2DConf = def
    { defaultTiledNavigation = centerNavigation
    , floatNavigation        = centerNavigation
    , screenNavigation       = lineNavigation
    , layoutNavigation       = [("Full", centerNavigation)
                               ,("Simple Tabs", lineNavigation)
                               ]
    , unmappedWindowRect     = [("Full", singleWindowRect)
                               ,("Simple Tabs", singleWindowRect)
                               ]
    }

data FULLBAR = FULLBAR deriving (Read, Show, Eq, Typeable)
instance Transformer FULLBAR Window where
    transform FULLBAR x k = k barFull (\_ -> x)

barFull = avoidStruts $ Simplest

myLayoutHook = showWorkspaceName
             $ onWorkspace wsFLOAT floatWorkSpace
             $ onWorkspace wsDEV threeCol
             $ onWorkspace wsWEB oneUp
             $ onWorkspace wsWRK smartTallTabbed
             $ onWorkspace wsRW flexiCombinators
             $ onWorkspace wsAV oneBig
             $ fullscreenFloat
             $ fullScreenToggle
             $ fullBarToggle
             $ mirrorToggle
             $ reflectToggle
             $ trackFloating
             $ flex ||| threeCol ||| tabs ||| flexiCombinators ||| oneUp ||| oneBig ||| smartTallTabbed ||| masterTabbedDynamic ||| masterTabbedP ||| bsp
  where

    floatWorkSpace      = simplestFloat
    fullBarToggle       = mkToggle (single FULLBAR)
    fullScreenToggle    = mkToggle (single FULL)
    mirrorToggle        = mkToggle (single MIRROR)
    reflectToggle       = mkToggle (single REFLECTX)
    smallMonResWidth    = 1920
    showWorkspaceName   = showWName' myShowWNameTheme --variable name for showWName hook

    named n             = renamed [(XMonad.Layout.Renamed.Replace n)]
    trimNamed w n       = renamed [(XMonad.Layout.Renamed.CutWordsLeft w),
                                   (XMonad.Layout.Renamed.PrependWords n)]
    suffixed n          = renamed [(XMonad.Layout.Renamed.AppendWords n)]
    trimSuffixed w n    = renamed [(XMonad.Layout.Renamed.CutWordsRight w),
                                   (XMonad.Layout.Renamed.AppendWords n)]

    addTopBar           = noFrillsDeco shrinkText topBarTheme

    mySpacing           = spacing gap
    sGap                = quot gap 2
    myGaps              = gaps [(U, gap),(D, gap),(L, gap),(R, gap)]
    mySmallGaps         = gaps [(U, sGap),(D, sGap),(L, sGap),(R, sGap)]
    myBigGaps           = gaps [(U, gap*2),(D, gap*2),(L, gap*2),(R, gap*2)]

    -----------------------------------------------------------------------
    -- Flex Layouts                                                      --
    -----------------------------------------------------------------------

    flex = trimNamed 5 "Flex"
              $ avoidStruts
              $ windowNavigation
              $ addTopBar
              $ addTabs shrinkText myTabTheme
              $ subLayout [] (Simplest ||| Accordion ||| dragPane Horizontal (1/10) (1/2))
              $ ifWider smallMonResWidth wideLayouts standardLayouts
              where
                  wideLayouts = myGaps $ mySpacing
                      $ (suffixed "Wide 3Col" $ hiddenWindows $ ThreeColMid 1 (1/20) (1/2))
                    ||| (trimSuffixed 1 "Wide BSP" $ hiddenWindows emptyBSP)
                  standardLayouts = myGaps $ mySpacing
                      $ (suffixed "Std 2/3" $ hiddenWindows $ ResizableTall 1 (1/20) (2/3) [])
                    ||| (suffixed "Std 1/2" $ hiddenWindows $ ResizableTall 1 (1/20) (1/2) [])

    flexiCombinators = named "Flexi Combinators"
              $ avoidStruts
              $ ifWider smallMonResWidth wideScreen normalScreen
              where
                  wideScreen   = smartTall ****||* smartTabbed
                  normalScreen = magnifier smartTall ***||** smartTabbed

    -----------------------------------------------------------------------
    -- Master-Tabbed Dymamic                                             --
    -----------------------------------------------------------------------

    masterTabbedDynamic = named "Master-Tabbed Dynamic"
              $ ifWider smallMonResWidth masterTabbedWide masterTabbedStd

    masterTabbedStd = named "Master-Tabbed Standard"
              $ addTopBar
              $ avoidStruts
              $ gaps [(U, gap*2),(D, gap*2),(L, gap*2),(R, gap*2)]
              $ mastered (1/100) (2/3)
              $ gaps [(U, 0),(D, 0),(L, gap*2),(R, 0)]
              $ tabbed shrinkText myTabTheme

    masterTabbedWide = named "Master-Tabbed Wide"
              $ addTopBar
              $ avoidStruts
              $ gaps [(U, gap*2),(D, gap*2),(L, gap*2),(R, gap*2)]
              $ mastered (1/100) (1/4)
              $ gaps [(U, 0),(D, 0),(L, gap*2),(R, 0)]
              $ mastered (1/100) (2/3)
              $ gaps [(U, 0),(D, 0),(L, gap*2),(R, 0)]
              $ tabbed shrinkText myTabTheme

    -----------------------------------------------------------------------
    -- Smart Tall-Tabbed                                                 --
    -----------------------------------------------------------------------

    smartTallTabbed = named "Smart Tall-Tabbed"
              $ avoidStruts
              $ ifWider smallMonResWidth wideScreen normalScreen
              where
                  wideScreen   = combineTwoP (TwoPane 0.03 (3/4))
                                             (smartTall)
                                             (smartTabbed)
                                             (ClassName "qutebrowser")
                  normalScreen = combineTwoP (TwoPane 0.03 (2/3))
                                             (smartTall)
                                             (smartTabbed)
                                             (ClassName "qutebrowser")

    smartTall = named "Smart Tall"
              $ addTopBar
              $ mySpacing
              $ myGaps
              $ boringAuto
              $ ifWider smallMonResWidth wideScreen normalScreen
              where
                  wideScreen = reflectHoriz $ Tall 1 0.03 (2/3)
                  normalScreen = Mirror $ Tall 1 0.03 (4/5)

    smartTabbed = named "Smart Tabbed"
              $ addTopBar
              $ mySmallGaps
              $ tabbed shrinkText myTabTheme

    -----------------------------------------------------------------------
    -- Other Misc Layouts                                                --
    -----------------------------------------------------------------------

    tabs = named "Tabs"
              $ avoidStruts
              $ addTopBar
              $ addTabs shrinkText myTabTheme
              $ Simplest

    masterTabbedP = named "MASTER TABBED"
              $ addTopBar
              $ avoidStruts
              $ mySpacing
              $ myGaps
              $ mastered (1/100) (1/2) $ tabbed shrinkText myTabTheme

    threeCol = named "Unflexed"
              $ avoidStruts
              $ addTopBar
              $ myGaps
              $ mySpacing
              $ ThreeColMid 1 (1/10) (1/2)

    oneBig = named "1BG"
              $ avoidStruts
              $ addTopBar
              $ mySpacing
              $ myGaps
              $ OneBig (3/4) (3/4)

    oneUp = named "1UP"
              $ avoidStruts
              $ myGaps
              $ combineTwoP (ThreeCol 1 (3/100) (1/2))
                            (Simplest)
                            (Tall 1 0.03 0.5)
                            (ClassName "qutebrowser")

    bsp = named "BSP"
              $ borderResize (avoidStruts
              $ addTopBar
              $ mySpacing
              $ myGaps
              $ borderResize emptyBSP)

---------------------------------------------}}}
-- ACTIONS                                   {{{
------------------------------------------------

---------------------------------------------------------------------------
-- Urgency Hook                                                          --
---------------------------------------------------------------------------
data LibNotifyUrgencyHook = LibNotifyUrgencyHook deriving (Read, Show)

instance UrgencyHook LibNotifyUrgencyHook where
    urgencyHook LibNotifyUrgencyHook w = do
        name     <- getName w
        Just idx <- fmap (W.findTag w) $ gets windowset

        safeSpawn "notify-send" [show name, "workspace " ++ idx]

---------------------------------------------------------------------------
-- New Window Actions                                                    --
---------------------------------------------------------------------------
myManageHook :: ManageHook
myManageHook =
        manageSpecific
    <+> manageDocks
    <+> namedScratchpadManageHook myScratchPads
    <+> fullscreenManageHook
    <+> manageSpawn
    where
        manageSpecific = composeOne
            [ resource          =? "desktop_window"                    -?> doIgnore
            , resource          =? "stalonetray"                       -?> doIgnore
            , resource          =? "lxappearance"                      -?> doCenterFloat
            , resource          =? "blueman-manager"                   -?> doCenterFloat
            , resource          =? "vokoscreenNG"                      -?> doFloat
            , resource          =? "Terminator"                        -?> doCenterFloat
            --, className         =? "Notes"                             -?> doFloat
            , className         =? "Calc"                              -?> doFloat
            , transience
            , resource          =? "TaskellboardW"                     -?> doCenterFloat
            , resource          =? "TaskellboardH"                     -?> doCenterFloat
            , resource          =? "Music"                             -?> doCenterFloat
            , className         =? "electronplayer"                    -?> forceCenterFloat
            , className         =? "Sxiv"                              -?> forceCenterFloat
            , className         =? "manuskript"                        -?> doShift "RW"
            , isBrowserDialog                                          -?> forceCenterFloat
            , isRole            =? gtkFile                             -?> forceCenterFloat
            , isDialog                                                 -?> doCenterFloat
            , isRole            =? "pop-up"                            -?> doCenterFloat
            , isInProperty         "_NET_WM_WINDOW_TYPE"
                                   "_NET_WM_WINDOW_TYPE_SPLASH"        -?> doCenterFloat
            , resource          =? "console"                           -?> forceCenterFloat
            , isFullscreen                                             -?> doFullFloat
            , pure True                                                -?> tileBelow ]
        isBrowserDialog         = isDialog <&&> className =? myBrowserClass
        gtkFile                 = "GtkFileChooserDialog"
        isRole                  = stringProperty "WM_WINDOW_ROLE"
        -- insert WHERE and focus WHAT
        tileBelow = insertPosition Below Newer
        tileBelowNoFocus = insertPosition Below Older

---------------------------------------------------------------------------
-- X Event Actions                                                       --
---------------------------------------------------------------------------
myHandleEventHook = docksEventHook
                <+> refocusLastWhen myPred
                <+> fadeWindowsEventHook
                <+> dynamicTitle myDynHook
                <+> handleEventHook def
                <+> XMonad.Layout.Fullscreen.fullscreenEventHook
    where
        myPred = refocusingIsActive <||> isFloat
        myDynHook = composeAll
            [ --taskell --> forceCenterFloat
            --, isWorkHangouts --> insertPosition End Newer
            ]

---------------------------------------------------------------------------
-- Custom hook helpers                                                   --
---------------------------------------------------------------------------
forceCenterFloat :: ManageHook
forceCenterFloat = doFloatDep move
  where
    move :: W.RationalRect -> W.RationalRect
    move _ = W.RationalRect x y w h

    w, h, x, y :: Rational
    w = 1/3
    h = 1/2
    x = (1-w)/2
    y = (1-h)/2


addTagAndContinue :: Query Bool -> String -> MaybeManageHook
addTagAndContinue p tag = do
  x <- p
  when x (liftX . addTag tag =<< ask)
  return Nothing
---------------------------------------------}}}
-- LOG                                       {{{
------------------------------------------------
myLogHook h = do

    -- following block for copy windows marking
    copies <- wsContainingCopies
    let check ws | ws `elem` copies =
                   pad . xmobarColor color3 color1 . wrap "*" " "  $ ws
                 | otherwise = pad ws

    refocusLastLogHook
    fadeWindowsLogHook myFadeHook
    ewmhDesktopsLogHook
    dynamicLogWithPP $ def

        { ppCurrent             = xmobarColor active  "" . wrap "[" "]"
        , ppTitle               = xmobarColor active  "" . shorten 30
        , ppVisible             = xmobarColor color0  ""
        , ppUrgent              = xmobarColor color1  "" . wrap "!" "!"
        , ppHidden              = xmobarColor color9  "" . check
        , ppHiddenNoWindows     = xmobarColor color0  "" . const ""
        , ppSep                 = xmobarColor color1 color4 " :: "
        , ppWsSep               = "<fc=#586e75> | </fc>"
        , ppLayout              = xmobarColor color3  ""
        , ppOrder               = \(ws:l:t:ex) -> [ws,l]++ex++[t]
        , ppOutput              = hPutStrLn h
        , ppSort                = fmap
                                  (namedScratchpadFilterOutWorkspace.)
                                  (ppSort def)
        , ppExtras              = [windowCount] }


myFadeHook = composeAll
    [ opaque -- default to opaque
    , isUnfocused --> opacity 0.85
    , (className =? "Terminator") <&&> (isUnfocused) --> opacity 0.9
    , (className =? "URxvt") <&&> (isUnfocused) --> opacity 0.9
    , (className =? "electronplayer") <&&> (isUnfocused) --> opaque
    , fmap ("Google" `isPrefixOf`) className --> opaque
    , isDialog --> opaque
    --, isFloating  --> opacity 0.75
    ]

---------------------------------------------}}}

-- vim: ft=haskell:foldmethod=marker:foldlevel=0:expandtab:ts=4:shiftwidth=4
