# MY XMONAD KEYBINDINGS

*M4-F1 Show Keybindings*

| **System:**                |                                     |
| -------------------------: | :---------------------------------- |
| XF86AudioMute              | Mute/Unmute Volume                  |
| XF86AudioLowerVolume       | Decrease Volume                     |
| XF86AudioRaiseVolume       | Increase Volume                     |
| XF86MonBrightnessDown      | Decrease Screen Brightness          |
| XF86MonBrightnessUp        | Increase Screen Brightness          |
| M4-F7                      | Session Options                     |
| M4-F10                     | Lock Screen                         |
| M4-F11                     | Switch Keyboard Layouts             |
| M4-q                       | Restart XMonad                      |
| M4-C-q                     | Rebuild & Restart XMonad            |
| M4-Shift-q                 | Quit XMonad                         |
| M4-Shift-F1                | Display Keybindings                 |

| **Actions:**               |                                     |
| -------------------------: | :---------------------------------- |
| M4-XF86Display             | Display - Force Internal            |
| M4-o                       | Display (output) launcher           |
| M1-c                       | Toggle Compton                      |
| M1-p                       | Capture Screen - Full Screen        |
| M1-Shift-p                 | Capture Screen - Focused Window     |
| M1-C-p                     | Capture Screen - Area Select        |
| M1-r                       | Record Screen                       |
| M1-Shift-r                 | Record Screen - Area Select         |
| M1-t                       | Start Pomodoro Timer                |
| M1-Shift-t                 | Stop Pomodoro Timer                 |
| M4-slash                   | On-Screen Keys                      |
| M4-Shift-slash             | On-Screen Keys Settings             |
| M4-C-a                     | Notify X Clipboard Selection        |

| **Launchers:**             |                                     |
| -------------------------: | :---------------------------------- |
| M4-F2                      | ManPages (Rofi)                     |
| M4-F3                      | DuckSearch (Rofi)                   |
| M4-F4                      | Mount (Rofi)                        |
| M4-F5                      | Unmount (Rofi)                      |
| M4-F6                      | QR Code Launcher                    |
| M4-Return                  | Terminal (URxvt)                    |
| M4-space                   | App Launcher (Rofi)                 |
| M4-Shift-space             | Unicode Launcher (Rofi)             |
| M4-C-Shift-space           | Emoji Launcher (Rofi)               |
| M4-c                       | NSP Calc (Clac)                     |
| M1-n                       | NSP Console (Terminator)            |
| M4-m                       | NSP Music (NCMPCPP)                 |
| M4-n                       | NSP Notes (Vimwiki)                 |
| M4-t                       | NSP Tasks (Taskell)                 |
| M4-v                       | NSP Video (ElectronPlayer)          |
| M1-x                       | NSP Vokoscreen                      |
| M4-s                       | Cancel Submap                       |
| M4-KP_Home                 | Newsboat RSS                        |
| M4-KP_Left                 | Cmus Music Player                   |
| M4-KP_Up                   | Irssi                               |
| M4-KP_Right                | Wordgrinder                         |
| M4-KP_Down                 | Neomutt Mail                        |
| M4-KP_Prior                | Xmonad Config                       |
| M4-KP_Next                 | Htop                                |
| M4-KP_End                  | Lynx Browser                        |
| M4-KP_Begin                | Cli-Visualizer                      |
| M4-KP_Insert               | Ranger File Manager                 |
| M4-Shift-KP_Home           | LibreOffice                         |
| M4-Shift-KP_Left           | Atom IDE                            |
| M4-Shift-KP_Up             | Inkscape                            |
| M4-Shift-KP_Right          | Shotcut                             |
| M4-Shift-KP_Down           | Virt-Manager                        |
| M4-Shift-KP_Next           | Marker                              |
| M4-Shift-KP_End            | Qutebrowser Browser                 |
| M4-Shift-KP_Begin          | Gimp                                |
| M4-Shift-KP_Insert         | PCManFM                             |

| **Monitors:**              |                                     |
| -------------------------: | :---------------------------------- |
| M1-g                       | Create UltraWide Monitor            |
| M1-Shift-g                 | Delete UltraWide Monitor            |

| **Windows:**               |                                     |
| -------------------------: | :---------------------------------- |
| M4-h                       | Navigate Windows Focus Left         |
| M4-j                       | Navigate Windows Focus Down         |
| M4-k                       | Navigate Windows Focus Up           |
| M4-l                       | Navigate Windows Focus Right        |
| C-h                        | Move Window Left                    |
| C-j                        | Move Window Down in Stack           |
| C-k                        | Move Window Up in Stack             |
| C-l                        | Move Window Right                   |
| M4-p                       | Hide Window to Stack                |
| M4-Shift-p                 | Restore Window to Stack             |
| M4-b                       | Promote Window to Master            |
| M1-b                       | Rotate Slave Windows                |
| M4-d                       | Duplicate Window to All Workspaces  |
| M4-BackSpace               | Kill Window                         |
| M4-Shift-BackSpace         | Kill All Windows in Workspace       |
| M4-C-h                     | Merge w/Sublayout                   |
| M4-C-j                     | Merge w/Sublayout                   |
| M4-C-k                     | Merge w/Sublayout                   |
| M4-C-l                     | Merge w/Sublayout                   |
| M4-Shift-g                 | Merge All Into Sublayout            |
| M4-g                       | Un-Merge From Sublayout             |
| M4-apostrophe              | Navigate Tabs Down                  |
| M4-semicolon               | Navigate Tabs Up                    |
| C-apostrophe               | Swap Tab Down                       |
| C-semicolon                | Swap Tab Up                         |
| M1-Shift-a                 | Add Window Tags                     |
| M1-Shift-c                 | Shift Tagged Window to Workspace    |
| M1-Shift-d                 | Delete Window Tags                  |
| M4-z                       | J Focus Up                          |
| M4-z                       | K Focus Down                        |
| M4-z                       | M Focus Master                      |
| M4-z                       | U Focus Urgent                      |
| M4-Left                    | Navigate Screen Focus Left          |
| M4-Down                    | Navigate Screen Focus Down          |
| M4-Up                      | Navigate Screen Focus Up            |
| M4-Right                   | Navigate Screen Focus Right         |
| M4-C-Left                  | Move Window to Left Screen          |
| M4-C-Down                  | Move Window to Below Screen         |
| M4-C-Up                    | Move Window to Above Screen         |
| M4-C-Right                 | Move Window to Right Screen         |
| M4-Shift-Left              | Swap Workspace w/Left Screen        |
| M4-Shift-Down              | Swap Workspace w/Below Screen       |
| M4-Shift-Up                | Swap Workspace w/Above Screen       |
| M4-Shift-Right             | Swap Workspace w/Right Screen       |

| **Workspaces & Projects:** |                                     |
| -------------------------: | :---------------------------------- |
| M4-grave                   | Next Non-Empty Workspace            |
| M4-Shift-grave             | Prev Non-Empty Workspace            |
| M4-a                       | Toggle Last Workspace               |
| M4-w                       | Switch to/or Create Project         |
| M4-Shift-w                 | Shift to Project                    |
| M4-1                       | View Workspace GEN                  |
| M4-2                       | View Workspace WRK                  |
| M4-3                       | View Workspace SYS                  |
| M4-4                       | View Workspace XMD                  |
| M4-5                       | View Workspace GIT                  |
| M4-6                       | View Workspace DEV                  |
| M4-7                       | View Workspace MON                  |
| M4-8                       | View Workspace RW                   |
| M4-9                       | View Workspace TMP                  |
| M4-0                       | View Workspace (NSP)                |
| C-1                        | Move Window to Workspace GEN        |
| C-2                        | Move Window to Workspace WRK        |
| C-3                        | Move Window to Workspace SYS        |
| C-4                        | Move Window to Workspace XMD        |
| C-5                        | Move Window to Workspace GIT        |
| C-6                        | Move Window to Workspace DEV        |
| C-7                        | Move Window to Workspace MON        |
| C-8                        | Move Window to Workspace RW         |
| C-9                        | Move Window to Workspace TMP        |
| C-0                        | Move Window to Workspace (NSP)      |
| M4-C-Shift-1               | Copy Window to Workspace GEN        |
| M4-C-Shift-2               | Copy Window to Workspace WRK        |
| M4-C-Shift-3               | Copy Window to Workspace SYS        |
| M4-C-Shift-4               | Copy Window to Workspace XMD        |
| M4-C-Shift-5               | Copy Window to Workspace GIT        |
| M4-C-Shift-6               | Copy Window to Workspace DEV        |
| M4-C-Shift-7               | Copy Window to Workspace MON        |
| M4-C-Shift-8               | Copy Window to Workspace RW         |
| M4-C-Shift-9               | Copy Window to Workspace TMP        |
| M4-C-Shift-0               | Copy Window to Workspace (NSP)      |

| **Layout Management:**     |                                     |
| -------------------------: | :---------------------------------- |
| M4-Tab                     | Cycle All Layouts                   |
| M4-Shift-Tab               | Reset the Layout                    |
| M4-C-Tab                   | Cycle Sublayouts                    |
| M4-f                       | Fullscreen                          |
| M4-Shift-f                 | Fake Fullscreen                     |
| M4-y                       | Float Tiled Windows                 |
| M4-Shift-y                 | Tile All Floating Windows           |
| M4-r                       | Reflect/Rotate Window               |
| M4-Shift-r                 | Force Reflect Window (Even on BSP)  |
| C-Shift-h                  | Ctrl-H Passthrough                  |
| C-Shift-j                  | Ctrl-J Passthrough                  |
| C-Shift-k                  | Ctrl-K Passthrough                  |
| C-Shift-l                  | Ctrl-L Passthrough                  |

| **Resize:**                |                                     |
| -------------------------: | :---------------------------------- |
| M4-period                  | Increase Master Windows             |
| M4-comma                   | Decrease Master Windows             |
| M4-bracketleft             | Expand (Left on BSP)                |
| M4-Shift-bracketright      | Expand (Down on BSP)                |
| M4-Shift-bracketleft       | Expand (Up on BSP)                  |
| M4-bracketright            | Expand (Right on BSP)               |
| M4-C-bracketleft           | Shrink (Left on BSP)                |
| M4-C-Shift-bracketright    | Shrink (Down on BSP)                |
| M4-C-Shift-bracketleft     | Shrink (Up on BSP)                  |
| M4-C-bracketright          | Shrink (Right on BSP)               |
