#!/bin/zsh

# ╔═╗┌─┐┬─┐┌─┐┌─┐┬┬  ┌─┐
# ╔═╝├─┘├┬┘│ │├┤ ││  ├┤
#o╚═╝┴  ┴└─└─┘└  ┴┴─┘└─┘

# Description:  zprofile file. Runs on login.
# Author:       Stephen Smith
# Date:         20200511
# Notes:        Place in ~/.config/zsh

# Adds '~/.local/bin' and all subdirectories to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
export PATH="$HOME/.gem/ruby/2.7.0/bin/:$PATH"

# Default programs
export BIB="$HOME/Documents/LaTeX/bibliography.bib"
export BROWSER="linkhandler"
export EDITOR="nvim"
export FILE="ranger"
export READER="zathura"
export REFER="$HOME/Documents/referbib"
export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"
export TERMINAL="urxvt"
export TRUEBROWSER="qutebrowser"

# Sets XDG variables for compliance
# https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
# https://wiki.archlinux.org/index.php/XDG_Base_Directory
# XDG_RUNTIME_DIR is set by default through pam_systemd
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_DIRS="/etc/xdg"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_DIRS="/usr/local/share/:/usr/share/"
export XDG_DATA_HOME="$HOME/.local/share"

# Cleaning ~/
export ATOM_HOME="$XDG_DATA_HOME"/atom
export GEM_HOME="$XDG_DATA_HOME"/gem
export GEM_SPEC_CACHE="$XDG_CACHE_HOME"/gem
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export LESSKEY="$XDG_CONFIG_HOME"/less/lesskey
export LYNX_CFG="$XDG_CONFIG_HOME"/lynx/lynx.cfg
export LYNX_LSS="$XDG_DATA_HOME"/lynx/lynx.lss
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export RXVT_SOCKET="$XDG_RUNTIME_DIR"/urxvtd
export TASKDATA="$XDG_DATA_HOME"/task
export TASKRC="$XDG_CONFIG_HOME"/task/taskrc
export TIMEWARRIORDB="$XDG_DATA_HOME"/timew
#export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XDEFAULTS="$XDG_CONFIG_HOME"/X11/xdefaults
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XRESOURCES="$XDG_CONFIG_HOME"/X11/xresources
export ZDOTDIR="$XDG_CONFIG_HOME"/zsh
xrdb -merge ~/.config/X11/xresources

# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"

# If .config/shortcutrc doesn't exist then run shortcuts.
[ ! -f $XDG_CONFIG_HOME/shortcutrc ] && shortcuts >/dev/null 2>&1

# Switch escape and caps if tty:
sudo -n loadkeys ~/.local/bin/ttymaps.kmap 2>/dev/null
