# ┌─┐┌─┐┬ ┬┬─┐┌─┐
# ┌─┘└─┐├─┤├┬┘│
#o└─┘└─┘┴ ┴┴└─└─┘

#corona
#ufetch

# set PATH
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi


autoload -U colors && colors
PS1="🧜‍%B%F{136}[%f%F{166}YoLo🐧%f%F{125}]%f %F{33}%1~%f %F{136}$%f%b "

stty stop undef

HISTFILE=~/.cache/zsh/history
HISTSIZE=10000
SAVEHIST=10000

setopt autocd extendedglob nomatch notify share_history hist_find_no_dups hist_ignore_dups hist_ignore_space
unsetopt beep
bindkey -v
export KEYTIMEOUT=1

zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
autoload -Uz compinit
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Use !!<space> without retyping last command. Useful if you forgot sudo.
bindkey " " magic-space

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char
bindkey -s '^o' 'lfcd\n'
bindkey -s '^a' 'bc -l\n'
bindkey -s '^f' 'cd "$(dirname "$(fzf)")"\n'
bindkey '^[[P' delete-char

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
# Not supported in the "fish" shell.
#(cat ~/.cache/wal/sequences &)

# Alternative (blocks terminal for 0-3ms)
#cat ~/.cache/wal/sequences

# To add support for TTYs this line can be optionally added.
#source ~/.cache/wal/colors-tty.sh

# Built-in zsh git prompt with git tab-completion!
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' check-for-changes true
zstyle ':vcs_info:git:*' get-revision true
zstyle ':vcs_info:git:*' stagedstr "%B%{$fg[green]%}%b"
zstyle ':vcs_info:git:*' unstagedstr "%B%{$fg[red]%}%b"
zstyle ':vcs_info:git*+set-message:*' hooks git-untracked git-st git-stashed git-status
zstyle ':vcs_info:git:*' patch-format '%10>...>%p%<< (%n applied)'
zstyle ':vcs_info:git:*' formats "%{$fg[blue]%}[%b%{$fg[yellow]%}:%r]%{$fg[red]%}%m%c%u"
zstyle ':vcs_info:git:*' actionformats "%{$fg[blue]%}[%b%{$fg[yellow]%}:%r]%{$fg[green]%}(%a)%{$fg[red]%}%m%c%u"

# Hook to show untracked files
+vi-git-untracked(){
if [[ $(git rev-parse --is-inside-work-tree 2>/dev/null) == 'true' ]] && \
    git status --porcelain | grep '??' &>/dev/null; then
    #git ls-files --others --directory --exclude-standard; then
hook_com[staged]+="%{$fg[magenta]%}"
else
hook_com[staged]+=""
fi
}

# Hook to show +N/-N when your local branch is ahead-of or behind remote HEAD
+vi-git-st(){
local ahead behind
local -a gitstatus

ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
(( $ahead )) && gitstatus+=( "+${ahead}" )

behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)
(( $behind )) && gitstatus+=( "-${behind}" )

hook_com[misc]+=${(j:/:)gitstatus}
}

# Hook to show stashed files
+vi-git-stashed(){
local -a stashes
if [[ -s ${hook_com[base]}/.git/refs/stash ]]; then
    stashes=$(git stash list 2>/dev/null | wc -l)
    hook_com[unstaged]+="%{$fg[cyan]%}(${stashes} stashed) "
fi
}

+vi-git-status(){
STATUS=$(command git status 2>/dev/null | tail -n1)
if [[ $STATUS == 'nothing to commit, working tree clean' ]]; then
    hook_com[staged]+="%{$fg[green]%}"
fi
}

[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shortcutrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shortcutrc"
source ~/.config/zsh/.zshrc.aliases
source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
