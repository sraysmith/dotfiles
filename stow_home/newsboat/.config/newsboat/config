#╔╗╔┌─┐┬ ┬┌─┐┌┐ ┌─┐┌─┐┌┬┐
#║║║├┤ │││└─┐├┴┐│ │├─┤ │
#╝╚╝└─┘└┴┘└─┘└─┘└─┘┴ ┴ ┴

# GENERAL SETTINGS
show-read-feeds yes
show-read-articles yes
auto-reload yes
reload-time 120
delete-read-articles-on-quit yes
max-items 10000
text-width 80
confirm-exit no
cleanup-on-quit no
show-keymap-hint yes
notify-always yes
notify-program "notify-send"
notify-format "%n unread articles"

# DEFINE
browser "linkhandler"
external-url-viewer "urlscan -dc -r 'linkhandler {}'"
download-retries 4
pager internal
article-sort-order date-asc
datetime-format "%Y-%m-%d"

# TITLE FORMAT
feedlist-title-format "%N: (%u) Feeds"
articlelist-title-format "%N: (%u) %T [%U]"
searchresult-title-format "%N: (%u) Search"
filebrowser-title-format "%N: %f %?O?Open File&Save File?"
help-title-format "%N: Help"
selecttag-title-format "%N: Select Tag"
selectfilter-title-format "%N: Select Filter"
itemview-title-format "%N: %T"
urlview-title-format "%N: URLs"
dialogs-title-format "%N: Dialogs"

# KEYBINDINGS
bind-key j down
bind-key k up
bind-key j next articlelist
bind-key k prev articlelist
bind-key J next-feed articlelist
bind-key K prev-feed articlelist
bind-key G end
bind-key g home
bind-key d pagedown
bind-key u pageup
bind-key l open
bind-key h quit
bind-key a toggle-article-read
bind-key n next-unread
bind-key N prev-unread
bind-key D pb-download
bind-key U show-urls
bind-key x pb-delete

# COLORS
color listnormal red default dim
color listfocus default default bold
color listnormal_unread default default
color listfocus_unread default default bold
color info default default bold
color article default default

# HIGHLIGHTING
highlight feedlist "https?://[^ ]+" cyan default
highlight articlelist "[[:digit:]]{4}-[[:digit:]]{2}-[[:digit:]]{2}" cyan default
highlight article "https?://[^ ]+" cyan default
highlight article "^(Feed|Title|Author|Link|Date):" blue default
ignore-article "*" "title =~ \"Sponsor\""
ignore-article "*" "title =~ \"Sponsored\""
highlight-article "tags =~ \"security\"" red default bold
feedlist-format "%11u %t"
articlelist-format "%4i %f %D  %?T?|%-24T|  ?%t"

# MACROS
macro , open-in-browser
macro t set browser "tsp youtube-dl --add-metadata -ic"; open-in-browser ; set browser linkhandler
macro a set browser "tsp youtube-dl --add-metadata -xic -f bestaudio/best"; open-in-browser ; set browser linkhandler
macro v set browser "setsid nohup mpv"; open-in-browser ; set browser linkhandler
macro w set browser "w3m"; open-in-browser ; set browser linkhandler
macro p set browser "rofi-handler"; open-in-browser ; set browser linkhandler
macro c set browser "xsel -b <<<" ; open-in-browser ; set browser linkhandler
macro i set browser "feh %u"; open-in-browser ; set browser linkhandler

# Podboat Podcasting
download-path "~/Music/%h/%n"
max-downloads 3
player "cmus"
