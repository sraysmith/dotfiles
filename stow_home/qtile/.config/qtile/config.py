#╔═╗┌┬┐┌─┐┌─┐┬ ┬┌─┐┌┐┌┌─┐
#╚═╗ │ ├┤ ├─┘├─┤├┤ │││└─┐
#╚═╝ ┴ └─┘┴  ┴ ┴└─┘┘└┘└─┘
#╔═╗┌┬┐┬┬  ┌─┐  ╔═╗┌─┐┌┐┌┌─┐┬┌─┐
#║═╬╗│ ││  ├┤   ║  │ ││││├┤ ││ ┬
#╚═╝╚┴ ┴┴─┘└─┘  ╚═╝└─┘┘└┘└  ┴└─┘

# Stephen Smith stephen.r.smith2@gmail.com
# https://gitlab.com/sraysmith
# 2021/12/19
# Current as of QTile 0.19.0-1

## IMPORTS                                   {{{
#-----------------------------------------------
import json
import os
import psutil
import re
import socket
import subprocess

from typing import List  # noqa: F401

from libqtile import bar, layout, widget, hook, extension
from libqtile.config import Click, Drag, Group, Key, Match, Screen, DropDown, ScratchPad, KeyChord, Rule
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
#--------------------------------------------}}}
## VARIABLES                                 {{{
#-----------------------------------------------
mod = "mod4"
myBrowser = "/home/ssmith/.local/bin/browser"
#terminal = guess_terminal()
myTerm = "kitty"
myLauncher          = "rofi -show drun -modi 'drun,window,ssh' -sort -matching fuzzy -display-drun 'Applications' -display-window 'Windows' -display-ssh 'SSH'"
#myEmojiLauncher = "rofi-emoji"

def kill_all_windows():
    @lazy.function
    def __inner(qtile):
        for window in qtile.current_group.windows:
            window.kill()
    return __inner

def kill_all_windows_except_current():
    @lazy.function
    def __inner(qtile):
        for window in qtile.current_group.windows:
            if window != qtile.current_window:
                window.kill()
    return __inner
#--------------------------------------------}}}
## THEMES                                    {{{
#-----------------------------------------------
# Pywal Colors Theme
colors = os.path.expanduser('~/.cache/wal/colors.json')
colordict = json.load(open(colors))
ColorZ =(colordict['colors']['color0'])
ColorA=(colordict['colors']['color1'])
ColorB=(colordict['colors']['color2'])
ColorC=(colordict['colors']['color3'])
ColorD=(colordict['colors']['color4'])
ColorE=(colordict['colors']['color5'])
ColorF=(colordict['colors']['color6'])
ColorG=(colordict['colors']['color7'])
ColorH=(colordict['colors']['color8'])
ColorI=(colordict['colors']['color9'])
ColorJ=(colordict['colors']['color10'])
ColorK=(colordict['colors']['color11'])
ColorL=(colordict['colors']['color12'])
ColorM=(colordict['colors']['color13'])
ColorN=(colordict['colors']['color14'])
ColorO=(colordict['colors']['color15'])
#--------------------------------------------}}}
## KEYBINDINGS                               {{{
#-----------------------------------------------
keys = [

    # Move focus between windows.
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),

    # Swap windows.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(),
        desc="Move window up"),

    # Grow windows.
    Key([mod, "control"], "j", lazy.layout.shrink(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow(),
        desc="Grow window up"),

    ##--Swap/Split/Unsplit stacks.
    Key([mod, "shift"], "b", lazy.layout.rotate(), lazy.layout.flip()),
    Key([mod, "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),

    # Toggle layouts/groups
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
    Key([mod], "y", lazy.window.toggle_floating()),
    Key([mod], "grave", lazy.screen.next_group()),
    Key([mod], "a", lazy.screen.toggle_group()),

    # Kill Windows
    Key([mod], "BackSpace", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "shift"], "BackSpace", kill_all_windows()),
    Key([mod, "control", "shift"], "BackSpace", kill_all_windows_except_current()),

    # Prompts
    Key([mod, "shift"], "f", lazy.findwindow()),
    Key([mod], "g", lazy.switchgroup(),
        desc="Switch to group using a prompt widget"),
    Key([mod], "r", lazy.spawncmd(">"),
        desc="Spawn a command using a prompt widget"),
    Key([mod, "shift"], "r", lazy.run_extension(extension.DmenuRun(
        dmenu_prompt=">",
        dmenu_font="DroidSansMono Nerd Font-12",
        background="#15181A",
        foreground="#00FF00",
        selected_background="#079822",
        selected_foreground="#ffffff",))),

    ##--Scratchpads--##
    KeyChord([mod], "n", [
        Key([], "h", lazy.group['scratchpad'].dropdown_toggle('notesH')),
        Key([], "w", lazy.group['scratchpad'].dropdown_toggle('notesW')),
        ]),
    KeyChord([mod], "t", [
        Key([], "h", lazy.group['scratchpad'].dropdown_toggle('taskellH')),
        Key([], "w", lazy.group['scratchpad'].dropdown_toggle('taskellW')),
        ]),
    Key([mod], "c", lazy.group['scratchpad'].dropdown_toggle('calc')),
    Key([mod], "m", lazy.group['scratchpad'].dropdown_toggle('music')),
    Key([mod], "v", lazy.group['scratchpad'].dropdown_toggle('movies')),
    Key([mod], "x", lazy.group['scratchpad'].dropdown_toggle('term')),
    Key([mod], "z", lazy.group['scratchpad'].dropdown_toggle('qtile shell')),
    Key([mod], "p", lazy.window.togroup('scratchpad')),
    #Key([mod, "shift"], "p", lazy.window.()),

    #Group Applications
    KeyChord([mod], "w", [
        Key([], "w", lazy.spawn(myBrowser), lazy.spawn(myTerm + " --directory '/home/ssmith/Work'"), lazy.group["WRK"].toscreen()),
        Key([], "x", lazy.spawn(myTerm), lazy.spawn(myTerm), lazy.spawn(myTerm), lazy.group["SYS"].toscreen()),
        Key([], "q", lazy.spawn(myTerm + " --directory '/home/ssmith/.config/qtile'"), lazy.group["QWM"].toscreen()),
        Key([], "s", lazy.spawn("steam-runtime"), lazy.group["STM"].toscreen()),
        Key([], "g", lazy.spawn(myTerm + " --directory '/home/ssmith/.gitlab/dotfiles'"), lazy.spawn(myTerm + " ranger"), lazy.group["GIT"].toscreen()),
        Key([], "m", lazy.spawn(myTerm + " -T 'myhtop' htop"), lazy.group["MON"].toscreen()),
        Key([], "a", lazy.spawn(myTerm + " ncmpcpp -q"), lazy.spawn(myTerm + " cava"), lazy.spawn(myTerm + " pidi --display mpv --size '300'"), lazy.group["AV"].toscreen()),
        Key([], "d", lazy.spawn("/usr/lib/xscreensaver/spheremonics"), lazy.spawn("/usr/lib/xscreensaver/cubicgrid"), lazy.spawn("/usr/lib/xscreensaver/surfaces"), lazy.group["DMO"].toscreen()),

        ]),

    ##--Open Terminal Applications--##
    Key([mod], "Return", lazy.spawn(myTerm), desc="Launch my terminal"),
    Key([mod], "KP_Insert", lazy.spawn(myTerm+" ranger")),
    Key([mod], "KP_End", lazy.spawn(myTerm+" lynx")),
    Key([mod], "KP_Down", lazy.spawn(myTerm+" neomutt")),
    Key([mod], "KP_Page_Down", lazy.spawn(myTerm+" htop")),
    Key([mod], "KP_Left", lazy.spawn(myTerm+" cmus")),
    Key([mod], "KP_Begin", lazy.spawn(myTerm+" vis")),
    Key([mod], "KP_Right", lazy.spawn(myTerm+" wordgrinder /home/ssmith/Documents/wgss.wg")),
    Key([mod], "KP_Home", lazy.spawn(myTerm+" newsboat")),
    #Key([mod], "KP_Up", lazy.spawn("/home/ssmith/Downloads/sent/./sent /home/ssmith/Downloads/sent/present2")),
    Key([mod], "KP_Page_Up", lazy.spawn(myTerm+" nvim /home/ssmith/.config/qtile/config.py")),

    ##--Open GUI Applications--##
    Key([mod, "shift"], "KP_Insert", lazy.spawn("pcmanfm")),
    Key([mod, "shift"], "KP_End", lazy.spawn("browser")),
    #Key([mod, "shift"], "KP_Down", lazy.spawn("thunderbird")),
    #Key([mod, "shift"], "KP_Page_Down", lazy.spawn("vlc")),
    #Key([mod, "shift"], "KP_Left", lazy.spawn("gpmdp")),
    Key([mod, "shift"], "KP_Begin", lazy.spawn("atom")),
    Key([mod, "shift"], "KP_Right", lazy.spawn("gimp")),
    Key([mod, "shift"], "KP_Home", lazy.spawn("shotcut")),
    Key([mod, "shift"], "KP_Up", lazy.spawn("libreoffice")),
    Key([mod, "shift"], "KP_Page_Up", lazy.spawn("inkscape")),

    # Restart/Shutdown QTile
    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),

    # Audio
    Key([], "XF86AudioMute", lazy.spawn("/usr/bin/pulseaudio-ctl mute")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("/usr/bin/pulseaudio-ctl up")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("/usr/bin/pulseaudio-ctl down")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight +5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -5")),


    # Misc Functions
    Key([mod, "shift"], "F1", lazy.spawn("zathura /home/ssmith/.config/qtile/qkeys.pdf")),
    Key([mod], "F2", lazy.spawn("rofi-man")),
    Key([mod], "F6", lazy.spawn("sh xcqr")),
    Key([mod], "F10", lazy.spawn("sh colorlock")),
    Key([mod], "F11", lazy.spawn("kb_switcher")),
    Key(["mod1"], "c", lazy.spawn("comptog")),

    # Screenshots
    Key(["mod1"], "p", lazy.spawn("screenshot")),
    Key(["mod1", "shift"], "p", lazy.spawn("screenshot 'window'")),
    Key(["mod1", "control"], "p", lazy.spawn("screenshot 'area'")),

    # Screencast
    Key(["mod1"], "r", lazy.spawn("screencast")),
    Key(["mod1", "shift"], "r", lazy.spawn("screencast 'area'")),

    # Pomodoro
    Key(["mod1"], "t", lazy.spawn("touch /home/ssmith/.pomodoro_session")),
    Key(["mod1", "shift"], "t", lazy.spawn("rm /home/ssmith/.pomodoro_session")),

    # Screenkeys
    Key([mod], "slash", lazy.spawn("screenkey --no-systray")),
    Key([mod, "shift"], "slash", lazy.spawn("killall screenkey &> /dev/null")),

    # Launchers
    Key([mod], "space", lazy.spawn(myLauncher)),
    Key([mod, "shift"], "space", lazy.spawn("rofi-unicode")),
    Key([mod, "control", "shift"], "space", lazy.spawn("rofi-emoji")),

]
#--------------------------------------------}}}
## GROUPS                                    {{{
#-----------------------------------------------
#groups = [Group(i) for i in "'GEN', 'WRK', 'SYS', 'QWM', 'GIT', 'DEV', 'MON', 'RW', 'TMP'"]
groups = [Group("GEN", label='GEN'),
          Group("WRK", init=True, persist=True, label='WRK'),
          Group("SYS", init=True, persist=True, label='SYS'),
          Group("QWM", init=True, persist=True, label='QWM'),
          Group("GIT", init=True, persist=True, label='GIT'),
          Group("DEV", init=True, persist=True, label='DEV'),
          Group("MON", init=True, persist=True, label='MON'),
          Group("RW",  init=True, persist=True, label='RW'),
          Group("TMP", init=True, persist=True, label='TMP'),
          Group("AV",  init=True, persist=True, label='AV'),
          Group("DMO", init=True, persist=True, label='DMO'),
          Group("STM", init=True, persist=True, label='STM', matches=[Match(wm_class=('Steam'))]),
          Group("WEB", init=True, persist=True, label='WEB'),
          ScratchPad("scratchpad", [
              DropDown("term", myTerm, opacity=0.8),
              DropDown("calc", (myTerm + " clac"),
                  x=0.794, y=0.591, width=0.2, height=0.4, opacity=0.9,
                  on_focus_lost_hide=True),
              DropDown("music", (myTerm + " ncmpcpp"),
                  x=0.10, y=0.2, width=0.8, height=0.6, opacity=0.9,
                  on_focus_lost_hide=True),
              DropDown("notesH", (myTerm + " nvim /home/ssmith/vimwiki/home/index.md"),
                  x=0.10, y=0.2, width=0.8, height=0.6, opacity=0.9,
                  on_focus_lost_hide=True),
              DropDown("notesW", (myTerm + " nvim /home/ssmith/vimwiki/work/index.md"),
                  x=0.10, y=0.2, width=0.8, height=0.6, opacity=0.9,
                  on_focus_lost_hide=True),
              DropDown("taskellH", (myTerm + " nvim /home/ssmith/.local/share/taskell/home.md"),
                  x=0.10, y=0.2, width=0.8, height=0.6, opacity=0.9,
                  on_focus_lost_hide=True),
              DropDown("taskellW", (myTerm + " nvim /home/ssmith/.local/share/taskell/work.md"),
                  x=0.10, y=0.2, width=0.8, height=0.6, opacity=0.9,
                  on_focus_lost_hide=True),
              DropDown("movies", "dex /usr/share/applications/electronplayer.desktop",
                  x=0.10, y=0.2, width=0.8, height=0.6, opacity=0.9,
                  on_focus_lost_hide=True, match=Match(wm_class='electronplayer')),
              DropDown("qtile shell", (myTerm + " --hold qtile shell"),
                  x=0.05, y=0.4, width=0.9, height=0.6, opacity=0.9,
                  on_focus_lost_hide=True),
              ])

         ]

for i in groups[:9:]:
    keys.extend([
        Key([mod], str(groups.index(i)+1), lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),
        Key([mod, "shift"], str(groups.index(i)+1), lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        Key([mod, "control"], str(groups.index(i)+1), lazy.window.togroup(i.name, switch_group=False),
            desc="Move focused window to group but remain on current group {}".format(i.name)),
        ])
#--------------------------------------------}}}
## LAYOUTS                                   {{{
#-----------------------------------------------
layout_theme = dict(
        border_width=2,
        border_focus=ColorC,
        border_normal=ColorZ,
        margin=10,
        )

layouts = [
    layout.MonadTall(name="Flex 3/4", ratio=0.65, **layout_theme),
    layout.MonadTall(name="Flex 1/2", ratio=0.5, **layout_theme),
    layout.Max(),
    layout.Stack(num_stacks=2, **layout_theme),
    layout.Slice(side="left", width=192, name="gimp", role="gimp-toolbox",
        fallback=layout.Slice(side="right", width=256, role="gimp-dock",
        fallback=layout.Stack(num_stacks=1, **layout_theme))),
    #layout.Floating(),
]
#--------------------------------------------}}}
## WIDGETS                                   {{{
#-----------------------------------------------
widget_defaults = dict(
    font='Ubuntu Nerd Font',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(padding=10, text=""),
                widget.CurrentLayout(foreground=ColorD),
                widget.GroupBox(highlight_method='block', hide_unused='True', foreground=ColorG),
                widget.Prompt(background=ColorF, foreground=ColorA),
                widget.WindowName(foreground=ColorF),
                widget.Notify(),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Mpd2(),
                widget.Systray(),
                widget.OpenWeather(app_key='cde4092bbe5a12f769c8b6ff05ae7fa5', cityid='4919624', metric=False, format='{location_city}: {main_temp} °{units_temperature} {humidity}% {weather_details}', background=ColorL, foreground=ColorZ),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorG, foreground=ColorL),
                widget.Net(interface="wlp2s0", format=' {down}', padding=5, background=ColorG, foreground=ColorZ),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorF, foreground=ColorG),
                #widget.TextBox("my config", name="config"),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                widget.Memory(format=' {MemUsed: .0f} MB ', background=ColorF, foreground=ColorZ),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorD, foreground=ColorF),
                widget.CPU(format=' {freq_current}GHz {load_percent}%', background=ColorD, foreground=ColorZ),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorC, foreground=ColorD),
                widget.PulseVolume(background=ColorC, foreground=ColorZ),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorE, foreground=ColorC),
                widget.Battery(format='{char} {percent:2.0%} {hour:d}:{min:02d}', charge_char='', discharge_char='', background=ColorE),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorB, foreground=ColorE),
                #widget.BatteryIcon(background=ColorE),
                widget.Clock(format='%Y-%m-%d %a %I:%M %p', background=ColorB, foreground=ColorZ),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorH, foreground=ColorB),
                widget.Pomodoro(prefix_active='⏱️ ',  prefix_inactive='POMODORO', background=ColorH, notification_on=True),
                widget.TextBox(text='', padding=-15, fontsize=45, background=ColorN, foreground=ColorH),
                #widget.QuickExit(background=ColorN),
                #widget.TextBox(text='', padding=-15, fontsize=45, background=ColorZ, foreground=ColorN),
            ],
            24,
        ),
    ),
]
#--------------------------------------------}}}
## MOUSEBINDINGS                             {{{
#-----------------------------------------------
# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]
#--------------------------------------------}}}
## RULES                                     {{{
#-----------------------------------------------
dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='electronplayer'), #electronplayer
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True
#--------------------------------------------}}}
## HOOKS                                     {{{
#-----------------------------------------------
# HOOKS
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.client_new
def _swallow(window):
    pid = window.window.get_net_wm_pid()
    ppid = psutil.Process(pid).ppid()
    cpids = {
            c.window.get_net_wm_pid(): wid for wid, c in window.qtile.windows_map.items()
    }
    for i in range(5):
        if not ppid:
            return
        if ppid in cpids:
            parent = window.qtile.windows_map.get(cpids[ppid])
            parent.minimized = True
            window.parent = parent
            return
        ppid = psutil.Process(ppid).ppid()

@hook.subscribe.client_killed
def _unswallow(window):
    if hasattr(window, "parent"):
        window.parent.minimized = False

#--------------------------------------------}}}

wmname = "LG3D"

## TEMPLATE                                  {{{
#-----------------------------------------------

#--------------------------------------------}}}
## vim: ft=python:foldmethod=marker:foldlevel=0:expandtab:ts=4:shiftwidth=4
