#!/bin/sh

#╔═╗┬ ┬┌┬┐┌─┐┌─┐┌┬┐┌─┐┬─┐┌┬┐
#╠═╣│ │ │ │ │└─┐ │ ├─┤├┬┘ │
#╩ ╩└─┘ ┴ └─┘└─┘ ┴ ┴ ┴┴└─ ┴


feh --bg-scale no-fehbg ~/.config/wall.png &
#compton --backend glx --vsync opengl-swc --paint-on-overlay &
#nitrogen --restore &
picom &
#urxvtd -q -o -f &
#sleep 3
#nm-applet &
unclutter --timeout 3 --fork &
xsetroot -cursor_name pirate &
systemctl --user start xcape
sleep 1 &
sh init-keyboard &
pcmanfm -d &
dunst -config ~/.config/dunst/dunstrc &
torstart &
mopidy &
mconnect -d &
