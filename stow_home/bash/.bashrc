# ┌┐ ┌─┐┌─┐┬ ┬┬─┐┌─┐
# ├┴┐├─┤└─┐├─┤├┬┘│
#o└─┘┴ ┴└─┘┴ ┴┴└─└─┘


# Taskwarrior status in prompt
#URGENT=""
#OVERDUE=""
#DUETODAY=""
#DUETOMORROW=""

#function task_indicator {
#    if [ `task +READY +OVERDUE count` -gt "0" ]; then
#        echo "$OVERDUE"
#    elif [ `task +READY +DUETODAY count` -gt "0" ]; then
#        echo "$DUETODAY"
#    elif [ `task +READY +TOMORROW count` -gt "0" ]; then
#        echo "$DUETOMORROW"
#    elif [ `task +READY urgency \> 10 count` -gt "0" ]; then
#        echo "$URGENT"
#    else
#        echo ' '
#    fi
#}

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Setting Bash prompt. Capitalizes username and host if root user (my root user uses this same config file).
if [ "$EUID" -ne 0 ] ; then
    #export PS1="⚓️\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]\$(task_indicator) "
    export PS1="⚓️\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]\u\[$(tput setaf 2)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
else
	export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 3)\]ROOT\[$(tput setaf 2)\]@\[$(tput setaf 4)\]$(hostname | awk '{print toupper($0)}') \[$(tput setaf 5)\]\W\[$(tput setaf 1)\]]\[$(tput setaf 7)\]\\$ \[$(tput sgr0)\]"
fi

if [ -e ~/.bashrc.aliases ] ; then
   source ~/.bashrc.aliases
fi

export EDITOR=nvim

# VI Mode
set -o vi

HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTTIMEFORMAT='%F %T '
HISTIGNORE='$HISTIGNORE:jrnl *'

# CD into directory by typing directory name.
shopt -s autocd
# Use !!<space> without retyping last command. Useful if you forgot sudo.
bind Space:magic-space

# Import colorscheme from 'wal' asynchronously
#(cat ~/.cache/wal/sequences &)

# Alternative (blocks terminal for 0-3ms)
#cat ~/.cache/wal/sequences

# To add support for TTYs this line can be optionally added.
source ~/.cache/wal/colors-tty.sh

# Git Status in Bash Prompt
GIT_PROMPT_ONLY_IN_REPO=1
#GIT_PROMPT_FETCH_REMOTE_STATUS=0                    # Uncomment to avoid fetching remote status
GIT_PROMPT_IGNORE_SUBMODULES=1                      # Uncomment to avoid searching for changed files in submodules
#GIT_PROMPT_WITH_VIRTUAL_ENV=0                       # Uncomment to avoid setting virtual environment infos for node/python/conda
GIT_PROMPT_SHOW_UPSTREAM=1                          # Uncomment to show upstream tracking branch
GIT_PROMPT_SHOW_UNTRACKED_FILES=normal              # Can be 'no', 'normal', or 'all'; determines counting of untracked files
GIT_PROMPT_SHOW_CHANGED_FILES_COUNT=0               # Uncomment to avoid printing the number of changed files
#GIT_PROMPT_STATUS_COMMAND=gitstatus_pre-1.7.10.sh   # Uncomment to support Git older than 1.7.10
GIT_PROMPT_START=...                                # Uncomment for custom prompt start sequence
GIT_PROMPT_END=...                                  # Uncomment for custom prompt end sequence
GIT_PROMPT_THEME=Custom                             # Use custom theme specified in file GIT_PROMPT_THEME_FILE
GIT_PROMPT_THEME_FILE=~/.git-prompt-colors.sh       # Choose theme. Default is ~/.git-prompt-colors.sh
GIT_PROMPT_THEME=Solarized                          # Use theme optimized for solarized color theme
source  /usr/lib/bash-git-prompt/gitprompt.sh

# Add support for shortcuts to directories
source ~/.config/shortcutrc

# Add gif to terminal
#gif-for-cli --rows `tput lines` --cols `tput cols` -l 5 https://media.giphy.com/media/j16BWCHEQZMOc/source.gif

# Get latest coronavirus stats
corona
