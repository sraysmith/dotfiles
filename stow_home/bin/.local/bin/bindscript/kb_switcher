#!/bin/sh

#╦╔═╔╗     ╔═╗┬ ┬┬┌┬┐┌─┐┬ ┬┌─┐┬─┐
#╠╩╗╠╩╗    ╚═╗││││ │ │  ├─┤├┤ ├┬┘
#╩ ╩╚═╝────╚═╝└┴┘┴ ┴ └─┘┴ ┴└─┘┴└─

# Description:  This script allows me to change keyboard layouts at will.
# Author:       Stephen Smith
# Date:         20190310
# Notes:        Requires xorg-xkbcomp and notify-send. Place in $HOME/.local/bin/bindscript/ directory.

# Switch between US, NO, DE, and RU layouts.

# If not explicitly set, select the next layout from the set [us, no, de, ru]

if [[ -n "$1" ]]; then
    setxkbmap $1

else
    layout=$(setxkbmap -query | awk 'FNR == 3 {print $2}')
    case $layout in
        us)
            setxkbmap no && notify-send -u Low "⌨ Norwegian / norsk Bokmål"
            ;;
        no)
            setxkbmap de && notify-send -u Low "⌨ German / Deutsch"
            ;;
        de)
            setxkbmap ru && notify-send -u Low "⌨ Russian / русский"
            ;;
        *)
            setxkbmap us && notify-send -u Low "⌨ English"
            ;;
    esac
fi
